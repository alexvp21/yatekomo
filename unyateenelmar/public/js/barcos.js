$(function(){
    $('#collapseBtn').on('click', function(){
        $('#collapseBtn i').toggleClass('fa-plus-square fa-minus-square');
    });

    var fecha = new Date();
    $(".calPicker").datepicker({
        dayNamesMin: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sab"],
        dateFormat: "yy-mm-dd",
        firstDay: 1
    });
    
    $('#iconosBarcos .hvr-underline-from-center').on('click', function(){
        $('#iconosBarcos .hvr-underline-from-center').removeClass('active');
        $(this).addClass('active');
    });

    $("#btnPickerA").on('click', function () {
        $("#calPickerA").datepicker('show');
    });

    $('#buscaBarco').on('click', function(e) {
        event.preventDefault();
        var targetForm = $(this).attr('data-target');
        var values = $('#'+targetForm).serializeArray();
        values = values.filter(valor => {
            if (valor.name != '_token') {
                return valor;
            }
        });
        localStorage.setItem('busqbarco', JSON.stringify(values));
        $('#'+targetForm).submit();
    });

    if (localStorage.busqbarco) {
        var barcos = JSON.parse(localStorage.busqbarco);
        barcos.forEach(element => {
            $('#advancedSearch [name=' + element['name'] + ']').val(element['value']);
        });
        localStorage.removeItem('busqbarco');
    } else {
        $(".calPicker").val($.datepicker.formatDate('yy-mm-dd', fecha));
    }

    $('.pictos .fas').tooltip({ placement: 'bottom'});
    
    $('.reservar').on('click', function(){
        event.preventDefault();
        var fechas = $('#advancedSearch').serializeArray();
        var parentForm = $(this).parent().parent();
        fechas.forEach(element => {
            if (element['name'] == 'fecha_inicio') {
                parentForm.children('.fecha-ini').val(element['value']);
            }
        });
        parentForm.submit();
    })
});
