$(function () {
    var fecha = new Date();

    $(".calPicker").datepicker({
        dayNamesMin: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sab"],
        monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        dateFormat: "yy-mm-dd",
        firstDay: 1
    });

    $(".calPicker").val($.datepicker.formatDate('yy-mm-dd', fecha));

    $("#welcomeSearchL, #welcomeSearchM").on('click', function(e){
        event.preventDefault();
        var targetForm = $(this).attr('data-target');
        var values = $('#'+targetForm).serializeArray();
        values = values.filter(valor => {
            if (valor.name != '_token') {
                return valor;
            }
        });
        localStorage.setItem('busqbarco', JSON.stringify(values));
        $('#'+targetForm).submit();
    });

    $("#btnPickerM").on('click', function () {
        $("#calPickerM").datepicker('show');
    });

    $("#btnPicker").on('click', function () {
        $("#calPickerL").datepicker('show');
    });

    $(".mini-form-call").on('click', function(){
        $(".rotate").toggleClass('is-flipped');
    });
})