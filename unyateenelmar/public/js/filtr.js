(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
const styles = {
    transitionOut: 'fadeOut',
    transitionIn: 'fadeIn',
    duration: '1',
    dNone: 'dnone'
}

const load = ({transition, duration}) => {
    switch (transition) {
        case 'fade':
            styles.transitionOut = 'fadeOut';
            styles.transitionIn = 'fadeIn';
        break;
    }
    if (duration != null && !isNaN(duration)) {
        styles.duration = duration;
    }
}

const fire = () => {
    const filters = document.querySelectorAll('.controls .filtr');

    filters.forEach(filter => {
        filter.addEventListener('click', e => {
            const attrFilter = e.target.getAttribute('data-cat');
            const elements = document.querySelectorAll('.filtr-elem');
            elements.forEach(filterElem => {
                setTimeout(() => {
                    filterElem.classList.remove(styles.dNone);
                }, styles.duration*100);
                if (attrFilter != 'all') {
                    let attributes = filterElem.getAttribute('data-filter').split(',');
                    if (!attributes.includes(attrFilter)) {
                        filterElem.classList.remove(styles.transitionIn);
                        filterElem.classList.add(styles.transitionOut);
                        setTimeout(() => {
                            filterElem.classList.add(styles.dNone);
                        }, styles.duration*100);
                    } else {
                        filterElem.classList.remove(styles.transitionOut);
                        filterElem.classList.add(styles.transitionIn, 'duration-'+styles.duration);
                    }
                } else {
                    filterElem.classList.remove(styles.transitionOut);
                    filterElem.classList.add(styles.transitionIn, 'duration-'+styles.duration);
                }
            })
        })
    })
}

module.exports = {
    fire: fire,
    load: load
}
},{}],2:[function(require,module,exports){
var filtr = require('./filtr');

document.addEventListener('DOMContentLoaded', () => {
    filtr.fire();
});
},{"./filtr":1}]},{},[2]);
