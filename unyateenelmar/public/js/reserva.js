$(function () {
    var counter = 1;
    var maxPasajeros = $('#max_pasajeros').val();
    var precioFinal;

    $('#precio_final').text(parseFloat($('#precio_barco').val())+'€');
    $("#fecha_compra").val($.datepicker.formatDate('yy-mm-dd', new Date()));

    $('.card-select').on('click', function () {
        var selElem = $(this).attr('data-select');
        var type = $(this).attr('data-type');
        var precio = $(this).attr('precio');
        switch (type) {
            case 'trayecto':
                $('#precio_trayecto').val(precio);
            break;
            case 'menu':
                $('#precio_menu').val(precio);
            break;
        }
        $('.' + type).removeClass('is-active');
        $('.' + type).find('.select-radio').removeAttr('checked');
        $(this).addClass('is-active');
        $(this).find('#' + selElem).attr('checked', true);
        actualizarPrecios();
    })
    
    $('.form-seleccion .accBtn').on('click', function () {
        event.preventDefault();
        if (counter < maxPasajeros) {
            actualizarPrecios('yes');            
            var tmpCount = counter++;
            var inGroup = $('<div>', { class: 'in-group mt-3' }).append(
                $('<input>', {
                    type: 'text',
                    name: 'nombre' + tmpCount,
                    class: 'pass-info',
                    placeholder: 'Nombre',
                    required: 'required'
                })
            ).append(
                $('<input>', {
                    type: 'text',
                    name: 'apellidos' + tmpCount,
                    class: 'pass-info',
                    placeholder: 'Apellidos',
                    required: 'required'
                })
            ).append(
                $('<input>', {
                    type: 'text',
                    name: 'dni' + tmpCount,
                    class: 'pass-info',
                    placeholder: 'DNI',
                    required: 'required'
                })
            ).append(
                $('<input>', {
                    type: 'text',
                    name: 'fechaNacimiento' + tmpCount,
                    class: 'pass-info',
                    placeholder: 'Fecha nacimiento',
                    required: 'required'
                }).datepicker({
                    changeMonth: true,
                    changeYear: true ,
                    dayNamesMin: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sab"],
                    dateFormat: "yy-mm-dd",
                    firstDay: 1,
                    monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    yearRange: '1990:2019'
                })
            ).append(
                $('<a>', {
                    class: 'btn btn-success accBtn'
                }).append(
                    $('<i>', {
                        class: 'far fa-minus-square'
                    })
                ).on('click', function () {
                    counter--;
                    $(this).parent().remove();
                    actualizarPrecios('no');
                })
            );
            $('#pasajeros').append(inGroup);
        }
    });

    var actualizarPrecios = function(opc) {
        var counter2 = counter;
        var tmpCount;
        switch (opc) {
            case 'yes':
                tmpCount = ++counter2;
            break;

            case 'no':
                tmpCount = --counter2;
            break;
        
            default:
                tmpCount = counter2;
            break;
        }
        precioFinal = (parseFloat($('#precio_trayecto').val()) 
        + parseFloat($('#precio_menu').val()) + 
        parseFloat($('#precio_barco').val()) + (parseFloat($('#precio_menu').val() * parseFloat(tmpCount))));
        $('#precio_final').text(precioFinal+'€');
    }

    $('#pagar').on('click', function(){
        event.preventDefault();
        $('#contador').val(counter);
        $('#precioTotal').val(precioFinal);
        $('#reservaForm').submit();
    })
})