$(function () {
    var show = false;

    $("#burger").on('click', function () {
        $(".nav-links").toggleClass('activeMenu');
    });

    window.onscroll = function () {
        if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
            document.getElementById("goUp").classList.add('d-block');
        } else {
            document.getElementById("goUp").classList.remove('d-block');
        }
    }

    document.getElementById('goUpB').addEventListener('click', function () {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    })

    $('#FormAS01 .pass i').click(function () {
        if (!show) {
            $("#FormAS01 .pass input").attr("type", "text");
            $("#FormAS01 .pass i").toggleClass("fa-eye fa-eye-slash");
            show = true;
        } else {
            $("#FormAS01 .pass input").attr("type", "password");
            $("#FormAS01 .pass i").toggleClass("fa-eye-slash fa-eye");
            show = false;
        }
    });

    $('#fecha-nacimiento').datepicker({
        changeMonth: true,
        changeYear: true ,
        dayNamesMin: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sab"],
        dateFormat: "yy-mm-dd",
        firstDay: 1,
        monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        yearRange: '1990:2019'
    });
});