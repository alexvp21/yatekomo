$(function () {
    $('.modificar').on('click', function (e) {
        var barcoId = e.target.getAttribute('data-barco');
        modificarBarco(barcoId);
    });

    $('#guardar').on('click', function () {
        $('#modBarco form').submit();
    });

    $('.eliminar').on('click', function (e) {
        Swal.fire({
            title: 'Estás seguro?',
            text: "No se puden revertir los cambios!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#083358',
            cancelButtonColor: '#dc3545',
            confirmButtonText: 'Sí, deseo borrar!'
        }).then((result) => {
            if (result.value) {
                var barcoId = e.target.getAttribute('data-barco');
                var form = $('<form>', {
                    action: '/delbarco/'+barcoId,
                    method: 'GET',
                    class: 'd-none'
                });

                $('body').append(form);
                form.submit();
                Swal.fire(
                    'Borrado!',
                    'El barco hizo aguas.',
                    'success'
                )
            }
        })
    });

    $('#addBarco').on('click', function () {
        $('#insBarco').modal('show');
    })
});

var modificarBarco = function (barcoId) {
    $.ajax({
        type: 'GET',
        dataType: 'JSON',
        url: '/barcoSpecs/' + barcoId,
        success: function (data) {
            var barco = data[0];
            populate('#modBarco form', barco);
            $("#modBarco").modal('show');
        }
    })
}

function populate(frm, data) {
    $.each(data, function (key, value) {
        $('[name=' + key + ']', frm).val(value);
    });
}