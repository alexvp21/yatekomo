$(function () {
    $('#tablaIncidencias').basictable({breakpoint:1160});
    
       $('.eliminar').on('click', function(e){
            var incidenciaId = e.target.getAttribute('data-incidencia');
            var form = $('<form>', {
                action: '/delincidencia/'+incidenciaId,
                method: 'GET',
                class: 'd-none'
            });
    
            $('body').append(form);
            form.submit();
        });

        $('#addIncidencia').on('click', function () {
            $('#insIncidencia').modal('show');
        })
});