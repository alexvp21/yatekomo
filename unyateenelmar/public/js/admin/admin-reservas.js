$(function () {

    $('#tablaReservas').basictable({breakpoint:1160});

    $('.eliminar').on('click', function(e){
        var reservaId = e.target.getAttribute('data-reserva');
        var form = $('<form>', {
            action: '/delreserva/'+reservaId,
            method: 'GET',
            class: 'd-none'
        });

        $('body').append(form);
        form.submit();
    });

    $('.modificar').on('click', function (e) {
        var reservaId = e.target.getAttribute('data-reserva');
        modificarReserva(reservaId);
    });

    $('#guardar').on('click', function () {
        $('#modReserva  form').submit();
    });
});
    

    var modificarReserva = function (reservaId) {
        $.ajax({
            type: 'GET',
            dataType: 'JSON',
            url: '/reservaSpec/' + reservaId,
            success: function (data) {
                var reserva = data[0];
                populate('#modReserva form', reserva);
                $("#modReserva").modal('show');
            }
        })
    }
    
    function populate(frm, data) {
        $.each(data, function (key, value) {
            $('[name=' + key + ']', frm).val(value);
        });
    }


