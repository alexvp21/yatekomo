
$(function () {

    $('#tablaUsuarios').basictable({breakpoint:1160});

    $('.modificar').on('click', function (e) {
        var clienteId = e.target.getAttribute('data-rol');
        modificarCliente(clienteId);
    });

    $('#guardar').on('click', function () {
        $('#modCliente form').submit();
    });

    $('.eliminar').on('click', function(e){
        var usuarioId = e.target.getAttribute('data-rol');
        var form = $('<form>', {
            action: '/delusuario/'+usuarioId,
            method: 'GET',
            class: 'd-none'
        });

        $('body').append(form);
        form.submit();
    });
});

var modificarCliente = function (clienteId) {
    $.ajax({
        type: 'GET',
        dataType: 'JSON',
        url: '/clienteSpecs/' + clienteId,
        success: function (data) {
            var cliente = data[0];
            populate('#modCliente form', cliente);
            $("#modCliente").modal('show');
        }
    })
}

function populate(frm, data) {
    $.each(data, function (key, value) {
        $('[name=' + key + ']', frm).val(value);
    });
}