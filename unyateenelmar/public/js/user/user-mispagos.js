$(function () {
    // $("#mostrarOcultarSeccionTarjetaCredito").click(function () {
    //     $("#seccionTarjetaCredito").toggle(this.checked);
    // });
    //Activar el script de table responsive
    $('#misPagosTabla').basictable({
        breakpoint: 1160
    });

    $(".botonFactura").click(function (e) {
        //Recogo valor de input modificar
        var factura_completa = $(this).attr("data-factura");

        //Le passo el valor al modal
        //$("#id-en-modal").val(valor);
        // alert(factura_completa);

        factura_completa = JSON.parse(factura_completa);
        console.log(factura_completa)
        // Datos de la empresa
        $('#fecha_factura').val(factura_completa['fecha_de_compra']);

        // Datos del cliente
        $('#id_factura').val(factura_completa['factura_id']);
        $('#nombre_cliente').val(factura_completa['name']);
        $('#fecha_cliente').val(factura_completa['fechaNacimiento']);
        $('#dni_cliente').val(factura_completa['dni']);
        $('#email_cliente').val(factura_completa['email']);
        $('#telefono_cliente').val(factura_completa['telefono']);

        //Datos del barco barco_id
        $('#barco_id').val(factura_completa['barco_id']);

        //Datos total productos
        $('#nombreBarco').val(factura_completa['nombre_barco']);
        $('#trayecto').val(factura_completa['nombre_trayecto']);
        $('#dias').val(factura_completa['dias']);
        $('#precio').val(factura_completa['precio_dia']);
        $('#precioTotal').val(factura_completa['precio_dia']);

        //Datos trayecto
        $('#precioTrayecto').val(factura_completa['precioTrayecto']);
        $('#nombreTrayecto').val(factura_completa['nombre_trayecto']);
        $('#precioTotalTrayecto').val(factura_completa['precioTrayecto']);

        //Datos menu
        if(factura_completa['numPasajeros']==0)factura_completa['numPasajeros']=1;
        var precioTotalMenu = (factura_completa['numPasajeros'] * factura_completa['precioPorPersona']);
        $('#unidadesPasajeros').val(factura_completa['numPasajeros']);
        $('#nombre_menu').val(factura_completa['nombre_menu']);
        $('#precioMenu').val(factura_completa['precioPorPersona']);
        $('#precioTotalMenu').val(precioTotalMenu);


        // console.log(factura_completa['barco_id']);
        $("#modalFactura").modal();

        //precio total
        var total=factura_completa['precio_dia']+precioTotalMenu+factura_completa['precioTrayecto'];
        $('#subTotal').val(total);
        var iva=total*11/100;
        var totalFinal=total+iva;

        $("#totalFactura").val(totalFinal);


    });
});

