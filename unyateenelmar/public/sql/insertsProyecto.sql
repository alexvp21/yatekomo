-- IMAGEN BARCO --

-- BARCO (yate,velero,goletas,catamaranes)--

-- LANZAR DESDE AQUI ABAJO --

INSERT INTO barco (nombre_barco,eslora,camarote,tipo,pasajero_max,anyo,precio_dia,plazas_cama,fianza,voto_positivo,voto_negativo,imagen_portada)
VALUES ('Yate1','10.25',4,'yate',8,2002,250.50,5,50.50,50,30,'/img/img-inicio-2.jpg');

INSERT INTO barco (nombre_barco,eslora,camarote,tipo,pasajero_max,anyo,precio_dia,plazas_cama,fianza,voto_positivo,voto_negativo, imagen_portada)
VALUES ('Velero1','10.25',4,'velero',7,2005,250.50,5,50.50,50,30,'/img/img-inicio-3.jpg');

INSERT INTO barco (nombre_barco,eslora,camarote,tipo,pasajero_max,anyo,precio_dia,plazas_cama,fianza,voto_positivo,voto_negativo, imagen_portada)
VALUES ('Goleta1','10.25',4,'goletas',6,1980,250.50,5,50.50,50,30,'/img/img-inicio-5.jpg');

INSERT INTO barco (nombre_barco,eslora,camarote,tipo,pasajero_max,anyo,precio_dia,plazas_cama,fianza,voto_positivo,voto_negativo, imagen_portada)
VALUES ('Catamaranes','10.25',4,'catamaranes',5,1989,250.50,5,50.50,50,30,'/img/img-inicio-4.jpg');

-- FAVORITO --
INSERT INTO favorito (cliente_id,barco_id)
VALUES (1,1);

INSERT INTO favorito (cliente_id,barco_id)
VALUES (2,2);

-- CHEF --
INSERT INTO chef (nombre,apellidos,dni,telefono) VALUES ('Cocinero1','Apellido1','10000000-A',686121212);
INSERT INTO chef (nombre,apellidos,dni,telefono) VALUES ('Cocinero1','Apellido2','10000000-B',626335544);
INSERT INTO chef (nombre,apellidos,dni,telefono) VALUES ('Cocinero1','Apellido3','10000000-C',616232342);
INSERT INTO chef (nombre,apellidos,dni,telefono) VALUES ('Cocinero1','Apellido4','10000000-D',627343452);
INSERT INTO chef (nombre,apellidos,dni,telefono) VALUES ('Cocinero1','Apellido5','10000000-F',623452343);
-- TRAYECTO --
INSERT INTO trayecto (nombre_trayecto,precioTrayecto,horas,url_Imagen) VALUES ('Costa Brava',1050,8,'/uploads/trayectos/trayecto1.png');
INSERT INTO trayecto (nombre_trayecto,precioTrayecto,horas,url_Imagen) VALUES ('Navega Ibiza',820,6,'/uploads/trayectos/trayecto2.png');
INSERT INTO trayecto (nombre_trayecto,precioTrayecto,horas,url_Imagen) VALUES ('Cartagena',600,4,'/uploads/trayectos/trayecto3.png');
INSERT INTO trayecto (nombre_trayecto,precioTrayecto,horas,url_Imagen) VALUES ('A tu gusto',1700,8,'/uploads/trayectos/trayecto4.png');
-- FACTURA --
INSERT INTO factura (forma_de_pago,precio,fecha_de_compra,detalles)
VALUES ('tarjeta',200,NOW(),'Nombre del barco,numero pasajeros etc etc etc');

INSERT INTO factura (forma_de_pago,precio,fecha_de_compra,detalles)
VALUES ('paypal',500,NOW(),'Nombre del barco,numero pasajeros etc etc etc');

INSERT INTO factura (forma_de_pago,precio,fecha_de_compra,detalles)
VALUES ('tarjeta',100,NOW(),'Nombre del barcqweqwo,numero pasajeros etc etc etc');

INSERT INTO factura (forma_de_pago,precio,fecha_de_compra,detalles)
VALUES ('tarjeta',50,NOW(),'Nombre del barco2,numero pasajeros etc etc etc');

INSERT INTO factura (forma_de_pago,precio,fecha_de_compra,detalles)
VALUES ('paypal',11121,NOW(),'Nombre del barco3,numero pasajeros etc etc etc');

-- MENU --
INSERT INTO menu(nombre_menu,precioPorPersona,alergenos,entrantesMenu,primerosMenu,segundosMenu,postresMenu,bebidasMenu)
VALUES ('Menu Crucero Los 4 vientos',20,'Contiene: Marisco, gluten,trazas cacahuete.',' Chips de plátano con dip de frijoles negros','esferas crujientes de yuca, pollo y mojo verde | cóctel de langostinos, kétchup de tamarillo, tabasco, mango verde y casabe de yuca','magret de pato en hoja de plátano','panna cotta de lichis y coulis de flor de Jamaica','Marques de alella (Gran reserva), agua, refrescos');

INSERT INTO menu(nombre_menu,precioPorPersona,alergenos,entrantesMenu,primerosMenu,segundosMenu,postresMenu,bebidasMenu)
VALUES ('Menu El Veggie',35,'Contiene: Marisco, gluten,trazas cacahuete, trazas de limón','Crema de habitas y menta','trinxat de patata, espigalls y tempeh | alcachofas maceradas en naranja','seta maitake a la plancha | ravioli relleno de huevos eco y alcachofa','carquinyoli al sorbete de moscatel y trufa de chocolate','Vino de la casa "SOMOS", agua, refrescos');

INSERT INTO menu(nombre_menu,precioPorPersona,alergenos,entrantesMenu,primerosMenu,segundosMenu,postresMenu,bebidasMenu)
VALUES ('Menu El ultimo Mono 3',50,'Contiene: Carne, gluten,trazas mora roja, queso variedad Parmesano','Croquetas de Leopoldo | Tartar vegetal con mozzarella tebia | Micuit del narcís con frutos secos y confitura de higo','Salmón marinado con cremoso de confitados | Brandada de bacalao, kalamata y xips de plátano | Canelón de jabalo de caza con bechamel trufada, foie y setas','Costillar duroc lacado con patata al horno | Mar y montaña de pulpo teriyaki','Mar rojo con queso y arandanos al polvo de trufa','Gran periñón, Vega sicilia unico (1720), agua y refrescos');

INSERT INTO menu(nombre_menu,precioPorPersona,alergenos,entrantesMenu,primerosMenu,segundosMenu,postresMenu,bebidasMenu)
VALUES ('Sin menu',0,'Sin menu','Sin menu','Sin menu','Sin menu','Sin menu','Sin menu');
-- CAPITAN --
INSERT INTO capitan (nombre_capitan,apellidos,dni,telefono,email,licencia)
VALUES ('Capitan1','Apellido1','20000000-A',666111444,'capitan1@yatekomo.com','Licencia1');

INSERT INTO capitan (nombre_capitan,apellidos,dni,telefono,email,licencia)
VALUES ('Capitan2','Apellido2','20000000-B',655111334,'capitan2@yatekomo.com','Licencia2');

INSERT INTO capitan (nombre_capitan,apellidos,dni,telefono,email,licencia)
VALUES ('Capitan3','Apellido3','30000000-B',655111334,'capitan3@yatekomo.com','Licencia3');

INSERT INTO capitan (nombre_capitan,apellidos,dni,telefono,email,licencia)
VALUES ('Capitan4','Apellido4','40000000-B',655111334,'capitan4@yatekomo.com','Licencia4');

-- RESERVA --
INSERT INTO reserva (fecha_inicio,precioTotal,estado,trayecto_id,barco_id,chef_id,cliente_id,factura_id,numPasajeros,menu_id, capitan_id)
VALUES (STR_TO_DATE('22-06-2019', '%d-%m-%Y'),1,'pendiente',1,1,1,1,1,2,1,1);

INSERT INTO reserva (fecha_inicio,precioTotal,estado,trayecto_id,barco_id,chef_id,cliente_id,factura_id,numPasajeros,menu_id, capitan_id)
VALUES (STR_TO_DATE('23-06-2019', '%d-%m-%Y'),2,'pendiente',2,2,2,2,2,4,2,1);

-- INCIDENCIA --
INSERT INTO incidencia (hora_inicidencia,info,barco_id,capitan_id)
VALUES (STR_TO_DATE('24-06-2019 10:40:09 PM', '%d-%m-%Y %r'),'No quedan cervezas',1,1);

INSERT INTO incidencia (hora_inicidencia,info,barco_id,capitan_id)
VALUES (STR_TO_DATE('25-06-2019 10:40:09 PM', '%d-%m-%Y %r'),'Se me ha roto el motor',2,2);

-- PUNTO --
-- TRAYECTO 1 --
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES ('2.180804','41.362882',1,'Barcelona');
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES ('2.843142','41.686551',1,'Lloret de mar');
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES ('3.235591','41.893342',1,'Llafranch');
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES ('3.312806','42.315259',1,'Port Lligat');
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES ('3.030149','42.696681',1,'Argelès Plage');
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES ('3.038574','42.787809',1,'Le barcarès');

-- TRAYECTO 2 --
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES (2.180804,41.362882,2,'Barcelona');
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES (1.226625,41.097266,2,'Tarragona');
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES (2.515958,39.690904,2,'Bañalbufar');

-- TRAYECTO 3 --
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES (2.180804,41.362882,3,'Barcelona');
INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES (-0.316544,39.455035,3,'Valencia');

INSERT INTO punto(latitud,longitud,trayecto_id,nombre_punto)
VALUES (0,0,4,'Libre');

-- VALORACION --
INSERT INTO valoracion (opinion,cliente_id,barco_id,reserva_id)
VALUES ('Opinion numero 1: Se come muy bien',1,1,1);

INSERT INTO valoracion (opinion,cliente_id,barco_id,reserva_id)
VALUES ('Opinion numero 2: Todo estupendo!',2,2,2);

-- PASAJEROS --
INSERT INTO pasajeros (nombre,apellidos,fechaNacimiento,dni,reserva_id)
VALUES ('Pasajero1','Apellido1',30,'30000000-A',1);

INSERT INTO pasajeros (nombre,apellidos,fechaNacimiento,dni,reserva_id)
VALUES ('Pasajero2','Apellido2',22,'30000000-B',2);

-- ANYOS --
CREATE TABLE anyos (
  id int(11) NOT NULL AUTO_INCREMENT,
  anyo int(11) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO anyos(anyo)
VALUES(1990);
INSERT INTO anyos(anyo)
VALUES(1991);
INSERT INTO anyos(anyo)
VALUES(1992);
INSERT INTO anyos(anyo)
VALUES(1993);
INSERT INTO anyos(anyo)
VALUES(1994);
INSERT INTO anyos(anyo)
VALUES(1995);
INSERT INTO anyos(anyo)
VALUES(1996);
INSERT INTO anyos(anyo)
VALUES(1997);
INSERT INTO anyos(anyo)
VALUES(1998);
INSERT INTO anyos(anyo)
VALUES(1999);
INSERT INTO anyos(anyo)
VALUES(2000);
INSERT INTO anyos(anyo)
VALUES(2001);
INSERT INTO anyos(anyo)
VALUES(2002);
INSERT INTO anyos(anyo)
VALUES(2003);
INSERT INTO anyos(anyo)
VALUES(2004);
INSERT INTO anyos(anyo)
VALUES(2005);
INSERT INTO anyos(anyo)
VALUES(2006);
INSERT INTO anyos(anyo)
VALUES(2007);
INSERT INTO anyos(anyo)
VALUES(2008);
INSERT INTO anyos(anyo)
VALUES(2009);
INSERT INTO anyos(anyo)
VALUES(2010);
INSERT INTO anyos(anyo)
VALUES(2011);
INSERT INTO anyos(anyo)
VALUES(2012);
INSERT INTO anyos(anyo)
VALUES(2013);
INSERT INTO anyos(anyo)
VALUES(2014);
INSERT INTO anyos(anyo)
VALUES(2015);
INSERT INTO anyos(anyo)
VALUES(2016);
INSERT INTO anyos(anyo)
VALUES(2017);
INSERT INTO anyos(anyo)
VALUES(2018);
INSERT INTO anyos(anyo)
VALUES(2019);
INSERT INTO anyos(anyo)
VALUES(2020);
