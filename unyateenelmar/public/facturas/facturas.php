    <?php
    if ($_POST["generar_factura"] == "true") {

        //Recibir detalles de factura
        $id_factura = $_POST["id_factura"];
        $fecha_factura = $_POST["fecha_factura"];

        //Recibir los datos de la empresa
        $nombre_tienda = $_POST["nombre_tienda"];
        $direccion_tienda = $_POST["direccion_tienda"];
        $poblacion_tienda = $_POST["poblacion_tienda"];
        $provincia_tienda = $_POST["provincia_tienda"];
        $codigo_postal_tienda = $_POST["codigo_postal_tienda"];
        $telefono_tienda = $_POST["telefono_tienda"];
        $fax_tienda = $_POST["fax_tienda"];
        $identificacion_fiscal_tienda = $_POST["identificacion_fiscal_tienda"];

        //Recibir los datos del cliente
        $nombre_cliente = $_POST["nombre_cliente"];
        $fecha_cliente = $_POST["fecha_cliente"];
        $dni_cliente = $_POST["dni_cliente"];
        $email_cliente = $_POST["email_cliente"];
        $telefono_cliente = $_POST["telefono_cliente"];

        //Recibir los datos de los productos
        $iva = $_POST["iva"];
        // $gastos_de_envio = $_POST["gastos_de_envio"];
        $barco = "1";

        //DATOS BARCO
        $nombre_barco= $_POST['nombreBarco'];
        $precio=$_POST['precio'];
        $euro=iconv('UTF-8', 'ISO-8859-1//TRANSLIT', "€");
        $precioTotal =$precio;

        //DATOS MENU
        $unidades_personas = $_POST['unidadesPasajeros'];
        $nombre_menu = $_POST['nombre_menu'] ;
        $precioMenu = $_POST['precioMenu'] ;
        $precioTotalMenu = $_POST['precioTotalMenu'] ;


        //Datos trayecto
        $precio_Trayecto = $_POST['precioTrayecto'];
        $nombreTrayecto = $_POST['nombreTrayecto'];
        $precio_total_trayecto = $_POST['precioTotalTrayecto'];



        //variable que guarda el nombre del archivo PDF
        $archivo = "../facturas/factura-$id_factura.pdf";

        //Llamada al script fpdf
        require('fpdf.php');


        $archivo_de_salida = $archivo;

        $pdf = new FPDF();  //crea el objeto
        $pdf->AddPage();  //a�adimos una p�gina. Origen coordenadas, esquina superior izquierda, posici�n por defeto a 1 cm de los bordes.


        //logo de la tienda
        $pdf->Image('../yateFactura.jpg', 0, 0, 40, 40, 'JPG', 'http://php-estudios.blogspot.com');

        // Encabezado de la factura
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(190, 10, "FACTURA", 0, 2, "C");
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->MultiCell(190, 5, "Numero de factura: $id_factura" . "\n" . "Fecha: $fecha_factura", 0, "C", false);
        $pdf->Ln(2);

        // Datos de la tienda
        $pdf->SetFont('Arial', 'B', 12);
        $top_datos = 45;
        $pdf->SetXY(40, $top_datos);
        $pdf->Cell(190, 10, "Datos de la tienda:", 0, 2, "J");
        $pdf->SetFont('Arial', '', 9);
        $pdf->MultiCell(
            190, //posici�n X
            5, //posici�n Y
            "Empresa: " . $nombre_tienda . "\n" .
                "Direccion: " . $direccion_tienda . "\n" .
                "Poblacion: " . $poblacion_tienda . "\n" .
                "Codigo Postal: " . $codigo_postal_tienda . "\n" .
                "Telofono: " . $telefono_tienda . "\n" .
                "Fax: " . $fax_tienda . "\n" .
                "Indentificacion Fiscal: " . $identificacion_fiscal_tienda,
            0, // bordes 0 = no | 1 = si
            "J", // texto justificado 
            false
        );


        // Datos del cliente
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(125, $top_datos);
        $pdf->Cell(190, 10, "Datos del cliente:", 0, 2, "J");
        $pdf->SetFont('Arial', '', 9);
        $pdf->MultiCell(
            190, //posici�n X
            5, //posicion Y
            "Nombre completo: " . $nombre_cliente . "\n" .
                "Fecha nacimiento: " . $fecha_cliente . "\n" .
                "Dni: " . $dni_cliente . "\n" .
                "Email: " . $email_cliente . "\n" .
                "Telefono: " . $telefono_cliente . "\n",
            0, // bordes 0 = no | 1 = si
            "J", // texto justificado
            false
        );

        //Salto de l�nea
        $pdf->Ln(2);

        $precio_subtotal = 0; // variable para almacenar el subtotal
        $y = 115; // variable para la posici�n top desde la cual se empezar�n a agregar los datos
        $x = 0;

        $pdf->SetXY(50, 120);
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetFillColor(229, 229, 229); //Gris tenue de cada fila
        $pdf->SetTextColor(3, 3, 3); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno

        //El parámetro badera dentro de Cell: true o false
        //true: Llena  la celda con el fondo elegido
        //false: No rellena la celda
        $pdf->Cell(24, 7, utf8_decode('Unidades'), 1, 0, 'L', $bandera);
        $pdf->Cell(30, 7, utf8_decode('Producto'), 1, 0, 'L', $bandera);
        $pdf->Cell(24, 7, utf8_decode('Precio'), 1, 0, 'L', $bandera);
        $pdf->Cell(24, 7, utf8_decode('Total'), 1, 0, 'L', $bandera);

        $pdf->Ln();
        $bandera = !$bandera; //Alterna el valor de la bandera

        //Linea barco
        $pdf->setX(50);
        $pdf->Cell(24, 7, $barco, 1, 0, 'L', $bandera);
        $pdf->Cell(30, 7, $nombre_barco, 1, 0, 'L', $bandera);
        $pdf->Cell(24, 7, $precio.$euro, 1, 0, 'L', $bandera);
        $pdf->Cell(24, 7, $precioTotal.$euro, 1, 0, 'L', $bandera);
        //Salto de línea para generar otra fila
        $pdf->Ln();

        //Linea menu
        /* 
        //DATOS MENU
        $unidades_personas = $_POST['unidadesPasajeros'];
        $nombre_menu = $_POST['nombre_menu'] ;
        $precioMenu = $_POST['precioMenu'] ;
        $precioTotalMenu = $_POST['precioTotalMenu'] ;
        */
        $pdf->setX(50);
        $pdf->Cell(24, 7, $unidades_personas, 1, 0, 'L', $bandera);
        $pdf->Cell(30, 7, $nombre_menu, 1, 0, 'L', $bandera);
        $pdf->Cell(24, 7, $precioMenu.$euro, 1, 0, 'L', $bandera);
        $pdf->Cell(24, 7, $precioTotalMenu.$euro, 1, 0, 'L', $bandera);
        //Salto de línea para generar otra fila
        $pdf->Ln();

        //Linea trayecto
        /* 
        //Datos trayecto
        $precio_Trayecto = $_POST['precioTrayecto'];
        $nombreTrayecto = $_POST['nombreTrayecto'];
        $precio_total_trayecto = $_POST['precioTotalTrayecto'];

        */
        $pdf->setX(50);
        $pdf->Cell(24, 7, "1", 1, 0, 'L', $bandera);
        $pdf->Cell(30, 7, $nombreTrayecto, 1, 0, 'L', $bandera);
        $pdf->Cell(24, 7, $precio_Trayecto.$euro, 1, 0, 'L', $bandera);
        $pdf->Cell(24, 7, $precio_total_trayecto.$euro, 1, 0, 'L', $bandera);
        //Salto de línea para generar otra fila
        $pdf->Ln();
        //C�lculo del subtotal 	

        $precio_subtotal= $precio_total_trayecto + $precioTotalMenu + $precioTotal;
        // $precio_subtotal += $e_precio_unidad[$x] * $e_unidades[$x];
        $x++;

        // aumento del top 5 cm
        $y = $y + 5;
        // }

        //C�lculo del Impuesto
        $add_iva = $precio_subtotal * $iva / 100;

        //C�lculo del precio total
        $total_mas_iva = round($precio_subtotal + $add_iva , 2);


        // Cabecera
        $pdf->Ln(2);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(190, 5, "I.V.A: $iva %", 0, 1, "C");
        $pdf->Cell(190, 5, "Subtotal: $precio_subtotal Euros", 0, 1, "C");
        $pdf->Cell(190, 5, "TOTAL: " . $total_mas_iva . " Euros", 0, 1, "C");


        $pdf->Output('F', $archivo_de_salida); //cierra el objeto pdf

        //Creacion de las cabeceras que generar�n el archivo pdf
        header("Content-Type: application/download");
        header("Content-Disposition: attachment; filename=$archivo");
        header("Content-Length: " . filesize("$archivo"));
        $fp = fopen($archivo, "r");
        fpassthru($fp);
        fclose($fp);

        //Eliminaci�n del archivo en el servidor
        unlink($archivo);
    }
