<head>
    <link rel="stylesheet" href="../css/user/user-home.css">
</head>
@extends('layouts.usermaster')
@section('userContent')
<script src="../js/user/user-home.js"></script>
<div class="col-12 col-md-9 ">
    <h2>Mi perfil</h2>
    <div class="seccion">
        <!-- Seccio -->
        <!-- Seccio tu foto -->
        <div class="row">
            <div class="col-12">
                @if (session('statusOk'))
                <div class="alert alert-success">
                    {{ session('statusOk') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if (session('statusFail'))
                <div class="alert alert-danger">
                    {{ session('statusFail') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="imagenPerfil text-center">
                    <h2>
                        {{ $user->name }}'s Profile
                    </h2>
                    <img src="/uploads/avatars/{{ $user->avatar }}" class="imagenPerfil">

                    <form enctype="multipart/form-data" action="/profile" method="POST">
                        <label>Actualizar foto perfil</label>
                        <input type="file" name="avatar">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" class="pull-right btn btn-sm btn-primary">
                    </form>
                </div>

            </div>
        </div><!-- seccio tu foto-->
        <!-- Seccio formulari -->
        <div class="row">
            <div class="col-12">
                <form id="formDatosPersonales" method="POST" action="{{ action('userController@actualizarDatos') }}">
                    @csrf
                    <div class="row ">
                        <div class="form-group col-12 col-md-6">
                            <label for="name">Nombre y Apellidos</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"  value="{{$user->name}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="telefono">Telefono</label>
                        <input type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono"  value="{{$user->telefono}}">
                            @error('telefono')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12 col-md-6">
                            <label for="fechaNacimiento">Fecha nacimiento</label>
                            <input type="text" class="form-control pointer @error('fechaNacimiento') is-invalid @enderror" id="fechaNacimiento" name="fechaNacimiento" value="{{$user->fechaNacimiento}}">
                            @error('fechaNacimiento')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <div class="form-group">
                                <label for="sexo">Sexo</label>
                                <select class="form-control @error('sexo') is-invalid @enderror" name="sexo" placeholder="Sexo">
                                    <option>Hombre</option>
                                    <option>Mujer</option>
                                </select>
                                @error('sexo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="form-group col-12 col-md-6">
                            <label for="dni">DNI</label>
                        <input type="text" class="form-control @error('dni') is-invalid @enderror" placeholder="dni" size="10" maxlength="9" name="dni" pattern="(([X-Z]{1})([-]?)(\d{7})([-]?)([A-Z]{1}))|((\d{8})([-]?)([A-Z]{1}))" title="Introduzca el dni"  value="{{$user->dni}}" required>
                            @error('dni')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="email">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{$user->email}}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="botonesUserHome" id="userBtnModificarDatos">Modificar datos</button>
                </form>
            </div>
        </div><!-- Formulario -->
    </div><!-- Seccio -->

    <!-- Seccio -->
    <h2>Modifica tu contraseña</h2>
    <div class="seccion">
        <div class="row">
            <div class="col-12">
                @if (session('statusPasswordOk'))
                <div class="alert alert-success">
                    {{ session('statusPasswordOk') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if (session('statusPasswordFail'))
                <div class="alert alert-danger">
                    {{ session('statusPasswordFail') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
            </div>
        </div>
        <form class="row justify-content-center pl-3 mt-3" id="formularioContrasenya" method="POST" action="{{ action('userController@resetPassword') }}">
            @csrf
            <div class=" form-group col-12 col-md-4">
                <label for="userContrasenyaAntiga">Contraseña vieja</label><br>
                <input type="password" id="userContrasenyaAntiga" class="form-control" name="oldpass">
            </div>
            <div class="form-group col-12 col-md-4">
                <label for="userContrasenyaNueva">Contraseña nueva</label><br>
                <input type="password" id="userContrasenyaNueva" class="form-control" name="newpass">
            </div>
            <div class="form-group col-12 col-md-4">
                <label for="userRepiteContrasenya">Repite contraseña</label><br>
                <input type="password" id="userRepiteContrasenya" class="form-control" name="newpass2">
            </div>
            <div class="form-group col-12 col-md-12">
                <input type="submit" id="enviarContrasenya" class="botonesUserHome">
            </div>


    </div>

</div>

@endsection
