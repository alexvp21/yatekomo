<head>
    <link rel="stylesheet" href="../css/user/user-misreservas.css">

</head>
@extends('layouts.usermaster')
@section('userContent')
<script src="../js/user/user-misreservas.js"></script>

<div class="col-md-9 col-12">
    <h2>Mi Reservas <i class="fas fa-book"></i></h2>
    <div class="seccion">
        <h2>Buscar</h2>
        <div class="row">
        </div>
    </div>
    <div class="seccion">
        <h2>Lista de reservas
        </h2>
        <!-- Seccio -->
        <!-- Seccio Lista Reservas -->
        <div class="row">
            <div class="col-9 col-md-12" id="seccionMisReservas">
                <table class="table rotar" id="tablaReserva">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre completo</th>
                            <th scope="col">Trayecto</th>
                            <th scope="col">Embarcacion</th>
                            <th scope="col">Fecha inicio</th>
                            <th scope="col">Chef</th>
                            <th scope="col">Menu</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Plazas_cama</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i=0; $i <sizeof($reservas); $i++) <tr>
                            <th scope="row">{{$i}}</th>
                            <td>{{$reservas[$i]['name']}}</td>
                            <td>{{$reservas[$i]['trayecto_id']}}</td>
                            <td>{{$reservas[$i]['nombre_barco']}}</td>
                            <td>{{$reservas[$i]['fecha_inicio']}}</td>
                            <td>{{$reservas[$i]['nombre']}}</td>
                            <td>{{$reservas[$i]['menu_id']}}</td>
                            <td>{{$reservas[$i]['precioTotal']}}€</td>
                            <td>{{$reservas[$i]['plazas_cama']}}</td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
