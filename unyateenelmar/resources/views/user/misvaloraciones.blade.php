<head>
    <link rel="stylesheet" href="../css/user/user-misvaloraciones.css">
</head>
@extends('layouts.usermaster')
@section('userContent')
<script src="../js/user/user-misvaloraciones.js"></script>
<div class="col-md-9 col-12">
    <h2>Mis valoraciones <i class="far fa-comments"></i></h2>
    <div class="seccion">
        <h2>Lista de reservas
        </h2>
        <!-- Seccio -->
        <!-- Seccio Mis valoraciones -->
        <div class="row">
            <div class="col-9 col-md-12" id="seccionTarjetaCredito">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Barco</th>
                            <th scope="col">Comentario</th>
                            <th scope="col">Editar</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i=0; $i <sizeof($valoraciones); $i++) <tr>
                            <th scope="row">{{$i}}</th>
                            <td>{{$valoraciones[$i]['nombre_barco']}}</td>
                            <td>{{$valoraciones[$i]['opinion']}}</td>
                        <td><button type="button" class="botonOpinion" name="Editar" data-user="{{$valoraciones[$i]['id']}}" id-valoracion="{{$valoraciones[$i]['valoracion_id']}}" opinion_input="{{$valoraciones[$i]['opinion']}}">Editar</button></td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header justify-content-center">
                            <h4></span> Modificar login</h4><button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="padding:40px 50px;">
                            <form method="POST" action="{{ url('/actualizarOpinion') }}">
                                @csrf
                                <div class="form-group">
                                    <input type="hidden" id="id-en-modal" name="id-valoracion">
                                    <label for="opinion"><span class="glyphicon glyphicon-user"></span> Opinion</label>
                                <textarea type="" name="opinion_new" class="form-control" id="opinion" placeholder="Cambiar opinion"></textarea>
                                </div>
                                <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Guardar</button>
                            </form>
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection
