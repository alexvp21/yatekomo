<head>
    <link rel="stylesheet" href="../css/user/user-mispagos.css">
</head>
@extends('layouts.usermaster')
@section('userContent')
<script src="../js/user/user-mispagos.js"></script>
<div class="col-md-9 col-9">
    <h2>Mi pagos </h2>
    {{-- <div class="seccion">
        <h2>Tarjeta de credito <i class="far fa-credit-card"></i>
            <input type="checkbox" id="mostrarOcultarSeccionTarjetaCredito">
        </h2> --}}
    <!-- Seccio -->
    <!-- Seccio tu Tarjeta de credito -->
    {{-- <div class="row">
            <div class="col-12 col-md-9 " id="seccionTarjetaCredito" style="display:none">
                <div class="row">
                    <div class="col-4 col-md-4" id="seleccionTarjeta">
                        <label for="userPagosSeleccionTarjeta">Selecciona tarjeta</label>
                        <select class="form-control" id="userPagosSeleccionTarjeta">
                            <option>XXXX-XXXX-XXXX-XXXX #1</option>
                            <option>XXXX-XXXX-XXXX-XXXX #2</option>
                        </select>
                    </div>
                    <div class="col-2 col-md-2 numerosTarjeta">
                        <label for="userPagosNumero">XXXX</label>
                        <input type="text" class="form-control" id="userPagosNumero" placeholder="Numero de tarjeta">
                    </div>
                    <div class="col-2 col-md-2 numerosTarjeta">
                        <label for="userPagosNumero">XXXX</label>
                        <input type="text" class="form-control" id="userPagosNumero" placeholder="Numero de tarjeta">
                    </div>
                    <div class="col-2 col-md-2 numerosTarjeta">
                        <label for="userPagosNumero">XXXX</label>
                        <input type="text" class="form-control" id="userPagosNumero" placeholder="Numero de tarjeta">
                    </div>
                    <div class="col-2 col-md-2 numerosTarjeta">
                        <label for="userPagosNumero">XXXX</label>
                        <input type="text" class="form-control" id="userPagosNumero" placeholder="Numero de tarjeta">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-md-6" id="nombreTitular">
                        <label for="userPagosNumero">Titular</label>
                        <input type="text" class="form-control" id="userPagosNumero" placeholder="Nombre completo">
                    </div>
                    <div class="col-2 col-md-2" id="dataCaducidad">
                        <label for="userDataCaducidad">Data de cad.</label>
                        <input type="text" class="form-control" id="userDataCaducidad" placeholder="MM/AA">
                    </div>
                    <div class="col-2 col-md-2" id="userCvv">
                        <label for="userCodigoSeguridad">CVV</label>
                        <input type="text" class="form-control" id="userCodigoSeguridad" placeholder="CVV">
                    </div>
                </div>
            </div>
        </div> --}}
    {{-- </div>
    <div class="seccion">
        <h2>Pay pal <i class="fab fa-cc-paypal"></i>
            <input type="checkbox" id="mostrarOcultarPaypal">
        </h2>
        <!-- Seccio -->
        <!-- Seccio tu Paypal -->
        <div class="row">
            <div class="col-md-9 col-12">
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="GSMSMCT8X3H6Q">
                    <input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal, la forma rápida y segura de pagar en Internet.">
                    <img alt="" border="0" src="https://www.paypalobjects.com/es_ES/i/scr/pixel.gif" width="1" height="1">
                </form>
            </div>
        </div>
    </div> --}}
    <div class="seccion">
        <h2>Tus compras<i class="fas fa-shopping-cart"></i></h2>
        <!-- Seccio -->
        <!-- Seccio tus compras -->
        <div class="row">
            @isset($facturas)
            <div class="col-12 text-center">
                <table class="table" id="tablaMisPagos">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nº Factura</th>
                            <th scope="col">Forma de pago</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Nombre barco</th>
                            <th scope="col">Fecha Factura</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i=0; $i <sizeof($facturas); $i++) <tr>
                            <th scope="row">{{$i}}</th>
                            <td>{{$facturas[$i]['factura_id']}}</td>
                            <td>{{$facturas[$i]['forma_de_pago']}}</td>
                            <td>{{$facturas[$i]['precio']}}</td>
                            <td>{{$facturas[$i]['nombre_barco']}}</td>
                            <td>{{$facturas[$i]['fecha_de_compra']}}</td>
                            <td><button type="button" class="botonFactura btn btn-info" name="Editar" data-factura="{{ json_encode($facturas[$i]) }}">Ver factura</button></td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
                @endisset
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modalFactura" role="dialog">
            <div class="modal-dialog  modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header justify-content-center">
                        <h4></span> Factura</h4><button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body text-center" style="padding:40px 50px;">

                        <form method="post" action="../facturas/facturas.php">
                            <h3>datos de la factura</h3>
                            <button type="submit" class="btn btn-info">GENERAR FACTURA PDF</button>
                            <input type="hidden" id="barco_id">

                            <table class="mt-3">

                                <tr>
                                    <td>ID de factura:</td>
                                    <td><input type="text" name="id_factura" id="id_factura" size="5"></td>
                                </tr>
                                <tr>
                                    <td>fecha emisión de factura:</td>
                                    <!-- Falta añadir fecha factura de la tabla -->
                                    <td><input type="text" name="fecha_factura" id="fecha_factura" value="<?php echo date("d-m-Y"); ?>" size="15"></td>
                                </tr>
                                <tr>
                                    <td>Nombre de la empresa:</td>
                                    <td><input type="text" name="nombre_tienda" value="Yatekomo" size="15" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Dirección de la tienda:</td>
                                    <td><input type="text" name="direccion_tienda" value="Port Olimpic, Amarre 3547" size="15" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Población de la tienda:</td>
                                    <td><input type="text" name="poblacion_tienda" value="Barcelona" size="25" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Provincia de la tienda:</td>
                                    <td><input type="text" name="provincia_tienda" value="Barcelona" size="25" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Código Postal de la tienda:</td>
                                    <td><input type="text" name="codigo_postal_tienda" value="08005" size="5" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Teléfono de la tienda:</td>
                                    <td><input type="text" name="telefono_tienda" value="649 84 27 27" size="15" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Fax de la tienda:</td>
                                    <td><input type="text" name="fax_tienda" value="0998 455879" size="15" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Identificacion Fiscal de la tienda:</td>
                                    <td><input type="text" name="identificacion_fiscal_tienda" value="B58818501" size="15" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr>
                                    </td>
                                    <td>
                                        <hr>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nombre completo del cliente:</td>
                                    <td><input type="text" id="nombre_cliente" name="nombre_cliente" value="Adria Grive Agusti" size="15" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Fecha nacimiento:</td>
                                    <td><input type="text" id="fecha_cliente" name="fecha_cliente" value="<?php echo date("d-m-Y"); ?>" size="25" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Dni del cliente:</td>
                                    <td><input type="text" id="dni_cliente" name="dni_cliente" value="47278742B" size="25" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Email del cliente:</td>
                                    <td><input type="text" id="email_cliente" name="email_cliente" value="agrive.ie@gmail.com" size="25" readonly="readonly"></td>
                                </tr>
                                <tr>
                                    <td>Telefono del cliente:</td>
                                    <td><input type="text" id="telefono_cliente" name="telefono_cliente" value="603842060" size="5" readonly="readonly"></td>
                                </tr>
                            </table>

                            <h3>PRODUCTOS</h3>

                            <table cellpadding="5" border="1" class="mt-3 mb-5" width="100%" class="justify-content-center">
                                <tr>
                                    <th>Unidades</th>
                                    <th>Producto</th>
                                    <th>Precio</th>
                                    <th>Precio total</th>
                                </tr>
                                <tr>
                                    <td><input type="text" name="unidades" id="unidades" value="1" size="3" readonly="readonly"></td>
                                    <td><input type="text" name="nombreBarco" id="nombreBarco" size="5" readonly="readonly"></td>
                                    <td><input type="text" name="precio" id="precio" size="3" readonly="readonly">€</td>
                                    <td><input type="text" name="precioTotal" id="precioTotal" size="3" readonly="readonly">€</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="unidadesPasajeros" id="unidadesPasajeros" size="4" readonly="readonly"></td>
                                    <td><input type="text" name="nombre_menu" id="nombre_menu" size="30" readonly="readonly">€</td>
                                    <td><input type="text" name="precioMenu" id="precioMenu" size="3" readonly="readonly">€</td>
                                    <td><input type="text" name="precioTotalMenu" id="precioTotalMenu" size="3" readonly="readonly">€</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="unidades" id="unidades" size="4" value="1" readonly="readonly"></td>
                                    <td><input type="text" name="nombreTrayecto" id="nombreTrayecto" size="25" readonly="readonly">€</td>
                                    <td><input type="text" name="precioTrayecto" id="precioTrayecto" size="3" readonly="readonly">€</td>
                                    <td><input type="text" name="precioTotalTrayecto" id="precioTotalTrayecto" size="3" readonly="readonly">€</td>
                                </tr>

                            </table>

                            <table cellpadding="5" border="1">
                                <tr>
                                    <td>Subtotal (sin IVA)</td>
                                    <td><input type="text" name="stotal" id="subTotal" size="5"> €</td>
                                </tr>
                                <tr>
                                    <td>Impuestos:</td>
                                    <td><input type="text" name="iva" value="11" size="5"> %</td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td><input type="text" name="total" id="totalFactura" size="5"> €</td>
                                </tr>
                            </table>
                            <br>
                            <p><mark>Si lo desea puede imprimir la factura.</mark></p>
                            <!-- <input type="hidden" name="unidades" value="<?php
                                                                                ?>">
                            <input type="hidden" name="productos" value="<?php
                                                                            ?>">
                            <input type="hidden" name="precio_unidad" value="<?php
                                                                                ?>"> -->
                            <!-- Campo que hace la llamada al script que genera la factura -->
                            <input type="hidden" name="generar_factura" value="true">
                        </form>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection