<head>
    <link rel="stylesheet" href="../css/user/user-favoritos.css">
</head>
@extends('layouts.usermaster')
@section('userContent')
<script src="../js/user/user-favoritos.js"></script>
<div class="col-md-9 col-12">
    <h2>Favoritos <i class="far fa-star elegido"></i></h2>
    <div class="seccion">
        <h2>Lista de Favoritos
        </h2>
        <!-- Seccio -->
        <!-- Seccio Lista Reservas -->
        <div class="row">
            <!--<div class="col-9 col-md-12" id="seccionFavoritos">-->
                    @isset($barcos)
                    @for ($i=0; $i <sizeof($barcos); $i++)
                    <div class="col-12 col-md-3 col-xl-4 text-center">
                            <div class="card text-white bg-light barcos-cartas">
                                <img src="{{$barcos[$i]['imagen_portada']}}" class="img-responsive" alt="Test Yate">
                                <div class="card-body">
                                    <h5 class="card-title">{{$barcos[$i]['nombre_barco']}}
                                        <span class="far fa-thumbs-up">{{$barcos[$i]['voto_positivo']}}</span>
                                        <span class="far fa-thumbs-down">{{$barcos[$i]['voto_negativo']}}</span>
                                    </h5>
                                    <p class="card-text">
                                        Eslora: {{$barcos[$i]['eslora']}}<br>
                                        Pasajeros Maximos: {{$barcos[$i]['pasajero_max']}}<br>
                                        Camarotes: {{$barcos[$i]['camarote']}}<br>
                                        Precio por dia: {{$barcos[$i]['precio_dia']}}€<br>
                                        Plazas cama: {{$barcos[$i]['plazas_cama']}}<br>
                                    </p>
                                </div>
                            </div>
                    </div>


                    @endfor
                    @endisset

                <!--</div>-->


            </div>
        </div>
    </div>
</div>
@endsection
