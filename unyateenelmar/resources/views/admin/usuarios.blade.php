<head>
    <link rel="stylesheet" href="../css/admin/admin-usuarios.css">
</head>
@extends('layouts.adminmaster')
@section('userContent')
<script src="../js/admin/admin-usuarios.js"></script>
<div class="col-md-9 col-12 main-body">
       
                @if (session('statusError'))
                    <div class="alert alert-danger">
                        {{ session('statusError') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if (session('statusOk'))
                    <div class="alert alert-success">
                        {{ session('statusOk') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
    <div class="seccion mt-5">
            <h2>Gestionar usuarios <i class="fas fa-user"></i> </h2>
        <div class="row">
               
            <div class="col-12">
                <!-- Zona de usuarios -->
                <table class="table" id="tablaUsuarios">
                    <thead>
                        <tr>
                            <th scope="col"># ID</th>
                            <th scope="col">Nombre </th> 
                            <th scope="col">Email</th> 
                            <th scope="col">Tipo usuario</th>
                            <th scope="col">Modificar Rol</th>
                            <th scope="col">Eliminar</th>
                            <!-- <th scope="col">Nº de reservas</th> -->
                        </tr>
                    </thead>
                    <tbody>
                            @isset($usuarios)
                            @for ($i=0; $i <sizeof($usuarios); $i++)
                        <tr style="border:1px solid black">
                            <th scope="row">{{$usuarios[$i]['id']}}</th>
                            <td> {{$usuarios[$i]['name']}}  </td>
                            <td> {{$usuarios[$i]['email']}}</td>
                            <td> {{$usuarios[$i]['rol']}}</td>
                            <td> <button class="w3-button w3-green modificar" data-rol="{{$usuarios[$i]['id']}}">Modificar</button>
                            </td>
                            <td> 
                            <button class="w3-button w3-red eliminar" data-rol="{{$usuarios[$i]['id']}}">Eliminar</button></td>
                        </tr>
                        @endfor
                        @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <form class="modal-dialog" role="document" method="post" action="{{ url('/modCliente') }}">
                @csrf
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modificar rol</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button> 
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <select name="rol" id="rol">
                            <option value="administrador" name="administrador" id="administrador">Administrador</option>
                            <option value="usuario" name="usuario" id="usuario">Usuario</option>
                            <input type="hidden" name="cliente_id">
                        </select>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="guardar">Guardar cambios</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
