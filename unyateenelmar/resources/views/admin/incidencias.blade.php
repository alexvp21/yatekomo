<head>
    <link rel="stylesheet" href="../css/admin/admin-incidencias.css">
</head>
@extends('layouts.adminmaster')
@section('userContent')
<script src="../js/admin/admin-incidencias.js"></script>
<div class="col-md-9 col-12">
        @if (session('statusError'))
        <div class="alert alert-danger">
            {{ session('statusError') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (session('statusOk'))
        <div class="alert alert-success">
            {{ session('statusOk') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="seccion">
        <h2>Gestionar incidencias <i class="fas fa-exclamation-triangle"></i>
        </h2>
        <!-- Seccion -->
        <!-- Seccion Incidencias -->
        <div class="row">

            <div class="col-md-12 col12" id="seccionTarjetaCredito">
                <table class="table" id="tablaIncidencias"> 
                    <thead>
                        <tr>
                            <th scope="col">Barco</th>
                            <th scope="col">Hora_incidencia</th>
                            <th scope="col">Capitan</th>
                            <th scope="col">Info</th>
                            <th scope="col">Resuelta</th>
                        </tr>
                    </thead>
                    <tbody>
                            @isset($incidencias)
                            @for ($i=0; $i <sizeof($incidencias); $i++)
                            <tr style="border:1px solid black">
                                <td> {{$incidencias[$i]['nombre_barco']}}  </td>
                                <td> {{$incidencias[$i]['hora_inicidencia']}}  </td>
                                <td> {{$incidencias[$i]['nombre_capitan']}}</td>
                                <td> {{$incidencias[$i]['info']}}</td>
                             
                                <td> 
                                        <button class="w3-button w3-red eliminar" data-incidencia="{{$incidencias[$i]['incidencias_id']}}">Eliminar</button></td>
                                </td>
                            </tr>
                            @endfor
                            @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <button class="btn btn-success" id="addIncidencia">Añadir Incidencia</button>

    <div class="modal fade" id="insIncidencia" tabindex="-1" role="dialog" aria-labelledby="insertIncidencia" aria-hidden="true">
        <form enctype="multipart/form-data" class="modal-dialog" method="post" action="{{ url('/insIncidencia') }}">
            @csrf
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="insertIncidencia">Insertar incidencia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="nombre">Id barco:</label>
                    <input class="form-control" type="text" name="barco_id" id="barco_id" />
                </div>
              <!--  <div class="form-group">
                    <label for="hora">Hora incidencia:</label>
                    <input class="form-control" type="text" name="hora_inicidencia" id="hora_inicidencia" />
                </div>-->
                <div class="form-group">
                    <label for="nombreCapitan">id capitan:</label>
                    <input class="form-control" type="text" name="capitan_id" id="capitan_id" />
                </div>
                <div class="form-group">
                    <label for="info">mensaje:</label>
                    <input class="form-control" type="text" name="info" id="info" />
                </div>
        
        
          
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar cambios</button>
            </div>
            </div>
        </form>
    </div>
</div>
@endsection