<head>
    <link rel="stylesheet" href="../css/admin/admin-reservas.css">
</head>
@extends('layouts.adminmaster')
@section('userContent')
<script src="../js/admin/admin-reservas.js"></script>
<div class="col-md-9 col-12">
        @if (session('statusError'))
        <div class="alert alert-danger">
            {{ session('statusError') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (session('statusOk'))
        <div class="alert alert-success">
            {{ session('statusOk') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
<div class="seccion">
        <h2>Gestionar Reservas <i class="fas fa-address-card"></i>
        </h2>
        <!-- Seccion -->
        <!-- Seccion Mis valoraciones -->
        <div class="row">
            <div class="col-md-12 col-12" id="seccionTarjetaCredito">
                <table class="table" id="tablaReservas">
                    <thead>
                        <tr>
                           
                            <th scope="col">fecha inicio </th>
                            <th scope="col">precioTotal</th>
                            <th scope="col">capitan</th>
                            <th scope="col"> barco</th>
                            <th scope="col"> cliente</th>
                            <th scope="col">estado</th>
                            <th scope="col"></th>
                            <th scope="col"> Eliminar Reserva</th>
                        </tr>
                    </thead>
                    <tbody>
                            @isset($reservas)
                            @for ($i=0; $i <sizeof($reservas); $i++)
                            <tr style="border:1px solid black">
                                <td> {{$reservas[$i]['fecha_inicio']}}  </td>
                                <td> {{$reservas[$i]['precioTotal']}} </td>
                                <td> {{$reservas[$i]['nombre_capitan']}}</td>
                                <td> {{$reservas[$i]['nombre_barco']}}</td>
                                <td>  {{$reservas[$i]['name']}}</td>
                                <td> {{$reservas[$i]['estado']}}</td>
                                <td> <button class="w3-button w3-green modificar" data-reserva="{{$reservas[$i]['reserva_id']}}">Modificar</button>
                                <td> 
                                    <button class="w3-button w3-red eliminar" data-reserva="{{$reservas[$i]['reserva_id']}}">Eliminar</button></td>
                                </td>
                            </tr>
                            @endfor
                            @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modReserva" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <form class="modal-dialog" role="document" method="post" action="{{ url('/modReserva') }}">
            @csrf
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modificar reserva</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button> 
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <select name="estado" id="estado">
                        <option value="pendiente" name="pendiente" id="pendiente">pendiente</option>
                        <option value="pagado" name="pagado" id="pagado">pagado</option>
                        <option value="finalizado" name="finalizado" id="finalizado">finalizado</option>
                        <input type="hidden" name="reserva_id">
                    </select>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="guardar">Guardar cambios</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
@endsection
