<head>
    <link rel="stylesheet" href="../css/admin/admin-valoraciones.css">
</head>
@extends('layouts.adminmaster')
@section('userContent')
    <script src="../js/admin/admin-valoraciones.js"></script>
    <div class="col-md-9 col-12">
   
    <div class="seccion">
        <h2>Gestionar valoraciones <i class="far fa-comments"></i>
        </h2>
        <!-- Seccion -->
        <!-- Seccion Mis valoraciones -->
        <div class="row">
            <div class="col-md-12 col-12">
                    @if (session('statusError'))
                    <div class="alert alert-danger">
                        {{ session('statusError') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if (session('statusOk'))
                    <div class="alert alert-success">
                        {{ session('statusOk') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <table class="table" id="tablaValoraciones">
                    <thead>
                          <tr style="border:1px solid black">
                            <th scope="col">Id_Valoracion</th>
                            <th scope="col">Barco</th>
                            <th scope="col">reserva/cliente</th>
                            <th scope="col">Comentario</th>
                            <th scope="col">Votos +</th>
                            <th scope="col">Votos -</th>
                            <th scope="col">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                            @isset($valoraciones)
                            @for ($i=0; $i <sizeof($valoraciones); $i++)
                            <tr style="border:1px solid black">
                                <th scope="row">{{$valoraciones[$i]['valoracion_id']}}</th>
                                <td> {{$valoraciones[$i]['nombre_barco']}}  </td>
                                <td> {{$valoraciones[$i]['reserva_id']}} / {{$valoraciones[$i]['name']}} </td>
                                <td> {{$valoraciones[$i]['opinion']}}</td>
                                <td> {{$valoraciones[$i]['voto_positivo']}}</td>
                                <td> {{$valoraciones[$i]['voto_negativo']}}</td>
                                <td> 
                                        <button class="w3-button w3-red eliminar" data-valoracion="{{$valoraciones[$i]['id']}}">Eliminar</button></td>
                                </td>
                            </tr>
                            @endfor
                            @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

