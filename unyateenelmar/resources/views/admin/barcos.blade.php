<head>
    <link rel="stylesheet" href="../css/admin/admin-barcos.css">
</head>
@extends('layouts.adminmaster')
@section('userContent')
<script src="../js/admin/admin-barcos.js"></script>
<div class="col-md-9 col-12 main-body">
    @if (session('statusError'))
        <div class="alert alert-danger">
            {{ session('statusError') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (session('statusOk'))
        <div class="alert alert-success">
            {{ session('statusOk') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="seccion mt-5">
            <h2>Gestionar barcos <i class="fas fa-ship"></i></h2>
            <button class="btn btn-success" id="addBarco">Añadir barco</button>
        <div class="row mt-4">
        <!-- Zona de cartas de barco -->
        @isset($barcos)
        @for ($i=0; $i <sizeof($barcos); $i++)
        <div class="col-12 col-md-4 text-center mt-3">
                <div class="card bg-light barcos-cartas">
                    <img src="{{$barcos[$i]['imagen_portada']}}" class="imgBarcos" alt="Test Yate">
                    <div class="card-body">
                        <h5 class="card-title">{{$barcos[$i]['nombre_barco']}}
                            <span class="far fa-thumbs-up">{{$barcos[$i]['voto_positivo']}}</span>
                            <span class="far fa-thumbs-down">{{$barcos[$i]['voto_negativo']}}</span>
                        </h5>
                        <p class="card-text">
                            Eslora: {{$barcos[$i]['eslora']}}<br>
                            Pasajeros Maximos: {{$barcos[$i]['pasajero_max']}}<br>
                            Camarotes: {{$barcos[$i]['camarote']}}<br>
                            Precio por dia: {{$barcos[$i]['precio_dia']}}€<br>
                            Plazas cama: {{$barcos[$i]['plazas_cama']}}<br>
                        </p>
                        <button class="btn btn-success modificar" data-barco="{{$barcos[$i]['barco_id']}}">Modificar</button>
                        <button class="btn btn-danger eliminar" data-barco="{{$barcos[$i]['barco_id']}}">Eliminar</button>
                    </div>
                </div>
        </div>
        @endfor
        @endisset

        <div class="modal fade" id="modBarco" tabindex="-1" role="dialog" aria-labelledby="updateBarco" aria-hidden="true">
            <form class="modal-dialog" method="post" action="{{ url('/modBarco') }}">
                @csrf
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="updateBarco">Modificar barco</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nombre">Nombre barco:</label>
                        <input type="text" name="nombre_barco" id="nombre" />
                    </div>
                    <div class="form-group">
                        <label for="eslora">Eslora:</label>
                        <input type="number" name="eslora" id="eslora" />
                    </div>
                    <div class="form-group">
                        <label for="pasajeros">Pasajeros máximos:</label>
                        <input type="number" name="pasajero_max" id="pasajeros" max="10" min="1" />
                    </div>
                    <div class="form-group">
                        <label for="precio">Precio/dia:</label>
                        <input type="text" name="precio_dia" id="precio" />
                    </div>
                    <div class="form-group">
                        <label for="camas">Plazas cama:</label>
                        <input type="number" name="plazas_cama" id="camas" />
                    </div>
                    <div class="form-group">
                        <label for="camarote">Camarotes:</label>
                        <input type="number" name="camarote" id="camarote" />
                    </div>
                    <input type="hidden" name="barco_id">
                    <div class="form-group">
                        <label for="anyo">Año:</label>
                        <select class="form-control" name="anyo">
                            @foreach($anyos as $id => $anyo)
                                <option value="{{$anyo}}">{{$anyo}}</option>
                            @endforeach
                        </select>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="guardar">Guardar cambios</button>
                </div>
                </div>
            </form>
        </div>
        <div class="modal fade" id="insBarco" tabindex="-1" role="dialog" aria-labelledby="insertBarco" aria-hidden="true">
            <form enctype="multipart/form-data" class="modal-dialog" method="post" action="{{ url('/insBarco') }}">
                @csrf
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="insertBarco">Insertar barco</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                     
                    <div class="form-group">
                        <label for="nombre">Nombre barco:</label>
                        <input class="form-control" type="text" name="nombre_barco" id="nombre" />
                    </div>
                    <div class="form-group">
                        <label for="tipo">Tipo:</label>
                        <select class="form-control" name="tipo" id="tipo" title="tipo de yate">
                            <option value="yate">Yate</option>
                            <option value="velero">Velero</option>
                            <option value="catamaranes">Catamaran</option>
                            <option value="goletas">Goleta</option>
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="eslora">Eslora:</label>
                        <input class="form-control" type="text" name="eslora" id="eslora" />
                    </div>
                    <div class="form-group">
                        <label for="pasajeros">Pasajeros máximos:</label>
                        <input class="form-control" type="number" name="pasajero_max" id="pasajeros" max="10" min="1" />
                    </div>
                    <div class="form-group">
                        <label for="precio">Precio/dia:</label>
                        <input class="form-control" type="text" name="precio_dia" id="precio" />
                    </div>
                    <div class="form-group">
                        <label for="camas">Plazas cama:</label>
                        <input class="form-control" type="number" name="plazas_cama" id="camas" />
                    </div>
                    <div class="form-group">
                        <label for="camarote">Camarotes:</label>
                        <input class="form-control" type="number" name="camarote" id="camarote" />
                    </div>
                    <div class="form-group">
                        <label for="anyo">Año:</label>
                        <select class="form-control" name="anyo">
                            @foreach($anyos as $id => $anyo)
                                <option value="{{$anyo}}">{{$anyo}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="imgBarco">Imagen del barco:</label>
                        <input type="file" name="imagen_portada" id="imgBarco" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar cambios</button>
                </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
