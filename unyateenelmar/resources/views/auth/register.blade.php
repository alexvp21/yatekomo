@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-5">
            <div id="FormAS01">
                <h3>Regístrate</h3>
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="form-group">
                        <input id="name" type="text" placeholder="Nombre"
                            class="form-control @error('name') is-invalid @enderror logInput" name="name"
                            value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="email" type="email" placeholder="Email"
                            class="form-control @error('email') is-invalid @enderror logInput" name="email"
                            value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="pass">
                            <input id="password" type="password" placeholder="Contraseña"
                                class="form-control @error('password') is-invalid @enderror logInput" name="password"
                                required autocomplete="current-password">
                            <i class="fas fa-eye"></i>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="password-confirm" type="password" placeholder="Repite contraseña"
                            class="form-control logInput" name="password_confirmation" required
                            autocomplete="new-password">
                    </div>

                    <div class="form-group">
                        <input id="form_dni" type="text" placeholder="DNI"
                            class="form-control @error('dni') is-invalid @enderror logInput" name="dni" required>
                        @error('dni')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="fecha-nacimiento" type="text" readonly placeholder="Fecha de nacimiento"
                            class="form-control @error('fecha_nacimiento') is-invalid @enderror logInput"
                            name="fecha_nacimiento" required>
                        @error('fecha_nacimiento')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <select name="sexo" id="sexo" class="logInput">
                            <option value="hombre">Hombre</option>
                            <option value="mujer">Mujer</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <input id="telefono" type="tel" placeholder="Número de teléfono"
                            class="form-control @error('telefono') is-invalid @enderror logInput" name="telefono"
                            required>
                        @error('telefono')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <button>{{ __('Registrarme') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection