<head>
    <link rel="stylesheet" href="css/welcome.css">
</head>
@extends('layouts.master')
@section('content')
<script src="js/welcome.js"></script>
<div class="container-fluid welcome-back">
    <div class="row">
        <div class="col-12 col-md-10 welcome-container-search">
                @if (session('statusFail'))
                    <div class="alert alert-danger">
                        {{ session('statusFail') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if (session('statusOk'))
                    <div class="alert alert-success">
                        {{ session('statusOk') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            <form id="barcoL" method="post" action="{{ url('/search') }}">
                @csrf
                <div class="cards d-none d-md-flex">
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Tipo</p>
                            <select class="form-control" name="tipo" title="tipo de yate">
                                <option value="todo">Todos</option>
                                <option value="yate">Yate</option>
                                <option value="velero">Velero</option>
                                <option value="catamaranes">Catamaran</option>
                                <option value="goletas">Goleta</option>
                            </select>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Eslora máxima</p>
                            <select class="form-control" name="eslora">
                                <option value="50">50m</option>
                                <option value="100">100m</option>
                                <option value="150">150m</option>
                                <option value="200">200m</option>
                            </select>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Pasajeros</p>
                            <input type="number" placeholder="Nº pasajeros" class="form-control" name="pasajero_max"
                                id="pasajeros" title="mínimo 2 pax" min="2" max="10" required="required" />
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Fecha inicio</p>
                            <div class="input-group-append">
                                <input type="text" name="fecha_inicio" readonly class="form-control date calPicker" id="calPickerL"
                                    placeholder="Selecciona una fecha" title="Escoge la fecha" />
                                <i class="fas fa-calendar-day fa-2x" id="btnPicker"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <button data-target="barcoL" id="welcomeSearchL" class="btn btn-success welcome-search d-none d-md-block mb-5" title="Busca tu yate">Buscar!</button>
            </form>
            <div class="welcome-container">
                <div class="rotate">
                    <div class="d-sm-flex d-md-none welcome-mini-busca face face-front">
                        <h1 class="welcome-mini-text">¿Buscas una embarcación?</h1>
                        <button class="btn btn-success welcome-search mini-form-call">Rellenar el formulario</button>
                    </div>
                    <form class="d-sm-grid d-md-none face face-back" id="barcoM"  method="post" action="{{ url('/search') }}">
                        @csrf
                        <div class="form-group welcome-mini-text">
                            <label for="tipoYateM">Tipo de yate</label>
                            <select class="form-control" name="tipo" id="tipoYateM" title="tipo de yate">
                                <option value="yate">Yate</option>
                                <option value="velero">Cliente</option>
                                <option value="catamaranes">Catamaran</option>
                                <option value="goletas">Goleta</option>
                            </select>
                        </div>
                        <div class="form-group welcome-mini-text">
                            <label for="pasajerosM">Pasajeros</label>
                            <input type="number" placeholder="Nº pasajeros" class="form-control" name="pasajero_max"
                                id="pasajerosM" title="Cuantos sois?" min="0" max="10" required />
                        </div>
                        <div class="form-group welcome-mini-text">
                            <label for="trayecto-mini">Eslora máxima</label>
                            <select class="form-control" name="eslora">
                                <option value="50">50m</option>
                                <option value="100">100m</option>
                                <option value="150">150m</option>
                                <option value="200">200m</option>
                            </select>
                        </div>
                        <div class="form-group welcome-mini-text">
                            <label for="calPickerM">Fecha inicio</label>
                            <div class="input-group-append">
                                <input name="fecha_inicio" type="text" readonly class="form-control date calPicker" id="calPickerM"
                                    placeholder="Selecciona una fecha" title="Escoge la fecha" />
                                <i class="fas fa-calendar-day fa-2x" id="btnPickerM"></i>
                            </div>
                        </div>
                        <a class="btn btn-warning mini-form-call" title="Volver atras">Atras</a>
                        <button dat-target="barcoM" id="welcomeSearchM" class="btn btn-success" title="Busca tu yate">Buscar!</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-12">
            <div class="social-network">
                <div class="texto">
                    <h1>Síguenos en nuestras redes sociales</h1>
                </div>
                <div class="iconos">
                    <i class="fab fa-3x fa-linkedin"></i>
                    <i class="fab fa-3x fa-twitter-square"></i>
                    <i class="fab fa-3x fa-instagram"></i>
                    <i class="fab fa-3x fa-facebook-square"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid barcos">
    <div class="row">
        <div class="col-12 barco">
            <div class="col-md-5 barco-texto">
                <h1>Yate</h1>
                <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Mauris porttitor massa erat, sed dapibus leo lobortis in.
                    Donec efficitur ipsum vel bibendum imperdiet. Curabitur
                    venenatis diam tincidunt leo scelerisque hendrerit.</h5>
            </div>
            <div class="col-md-5 barco-img yate">
            </div>
        </div>
    </div>
    <div class="row pt-4">
        <div class="col-12 barco">
            <div class="col-md-5 barco-texto">
                <h1>Velero</h1>
                <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Mauris porttitor massa erat, sed dapibus leo lobortis in.
                    Donec efficitur ipsum vel bibendum imperdiet. Curabitur
                    venenatis diam tincidunt leo scelerisque hendrerit.</h5>
            </div>
            <div class="col-md-5 barco-img velero">
            </div>
        </div>
    </div>
    <div class="row pt-4">
        <div class="col-12 barco">
            <div class="col-md-5 barco-texto">
                <h1>Catamaran</h1>
                <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Mauris porttitor massa erat, sed dapibus leo lobortis in.
                    Donec efficitur ipsum vel bibendum imperdiet. Curabitur
                    venenatis diam tincidunt leo scelerisque hendrerit.</h5>
            </div>
            <div class="col-md-5 barco-img catamaran">
            </div>
        </div>
    </div>
    <div class="row pt-4">
        <div class="col-12 barco">
            <div class="col-md-5 barco-texto">
                <h1>Goleta</h1>
                <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Mauris porttitor massa erat, sed dapibus leo lobortis in.
                    Donec efficitur ipsum vel bibendum imperdiet. Curabitur
                    venenatis diam tincidunt leo scelerisque hendrerit.</h5>
            </div>
            <div class="col-md-5 barco-img goleta">
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt-5 mb-3 text-center">
    <h1 class="mb-2 text-uppercase">Proceso de alquiler</h1>
    <div class="row welcome-alquiler-container">
        <div class="col-12 col-md-10 welcome-alquiler">
            <div class="proceso proceso-1">
                <h4 class="pt-4">Busca</h4>
            </div>
            <div class="proceso proceso-2">
                <h4 class="pt-4">Reserva</h4>
            </div>
            <div class="proceso proceso-3">
                <h4 class="pt-4">¡Disfruta!</h4>
            </div>
        </div>
    </div>
</div>
@endsection
