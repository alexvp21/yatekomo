<nav>
    <ul class="user-nav">
        <li>
            <div class="mini-user-nav">
                <div class="dropdown">
                    <i class="btn btn-secondary dropdown-toggle fas fa-bars fa-2x" href="javascript:void(0)"
                        role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"></i>

                    <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuLink">
                        <a href="/" class="dropdown-item">
                            <p>Inicio</p>
                        </a>
                        <a href="/barcos" class="dropdown-item">
                            <p>Barcos</p>
                        </a>
                        <a href="/quienesomos" class="dropdown-item">
                            <p>Quienes somos</p>
                        </a>
                        <a href="/contacto" class="dropdown-item">
                            <p>Contactanos</p>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="/user/home" class="dropdown-item">
                            <p>Mi usuario</p>
                        </a>
                        <a href="/user/favoritos" class="dropdown-item">
                            <p>Favoritos</p>
                        </a>
                        <a href="/user/mispagos" class="dropdown-item">
                            <p>Mis pagos</p>
                        </a>
                        <a href="/user/misreservas" class="dropdown-item">
                            <p>Mis reservas</p>
                        </a>
                        <a href="/user/misvaloraciones" class="dropdown-item">
                            <p>Mis valoraciones</p>
                        </a>
                    </div>
                </div>
            </div>
            <a href="/"><img src="../yate.png" alt="yate" class="ml-3" /></a>
        </li>
        <li>
            @auth
            <a id="navYate" class="ml-3 dropdown-toggle noLetterSpace" href="#" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu" aria-labelledby="navYate">
                <a class="noLetterSpace noLetterSpace-user-menu dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>

                <a class="noLetterSpace noLetterSpace-user-menu dropdown-item" href="{{ url('/user/home') }}">
                    {{ __('Mi zona') }}
                </a>
            </div>
            @endauth
        </li>
    </ul>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-3 user-nav nav-secondary d-none d-md-block">
            <ul class="user-links">
                <li>
                    <div class="dropdown">
                        <a class="btn btn-secondary dropdown-toggle" href="javascript:void(0)" role="button"
                            id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            La web
                        </a>

                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuLink">
                            <a href="/" class="dropdown-item">
                                <p>Inicio</p>
                            </a>
                            <a href="/barcos" class="dropdown-item">
                                <p>Barcos</p>
                            </a>
                            <a href="/quienesomos" class="dropdown-item">
                                <p>Quienes somos</p>
                            </a>
                            <a href="/contacto" class="dropdown-item">
                                <p>Contactanos</p>
                            </a>
                        </div>
                    </div>
                </li>
                <div>
                    <li>
                        <a href="/user/home">
                            <h5>Mi usuario</h5>
                        </a>
                    </li>
                    <li>
                        <a href="/admin/barcos">
                            <h5>Gestionar barcos</h5>
                        </a>
                    </li>
                    <li>
                        <a href="/admin/usuarios">
                            <h5>Gestionar usuarios</h5>
                        </a>
                    </li>
                    <li>
                        <a href="/admin/valoraciones">
                            <h5>Gestionar valoraciones</h5>
                        </a>
                    </li>
                    <li>
                        <a href="/admin/incidencias">
                            <h5>Gestionar incidencias</h5>
                        </a>
                    </li>
                    <li>
                        <a href="/admin/reservas">
                            <h5>Gestionar reservas</h5>
                        </a>
                    </li>
                </div>
            </ul>
        </div>
        @yield('userContent')
    </div>
</div>