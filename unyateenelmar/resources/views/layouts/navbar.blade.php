<nav>
  <div id="logo">
    <a href="/"><img src="../yate.png" alt="yate" class="navbar-brand" /></a>
  </div>
  <ul class="nav-links">
    <li>
      <a href="/"><h5>Inicio</h5></a>
    </li>
    <li>
      <a href="/barcos"><h5>Barcos</h5></a>
    </li>
    <li>
      <a href="/quienesomos"><h5>Quienes somos</h5></a>
    </li>
    <li>
      <a href="/contacto"><h5>Contactanos</h5></a>
    </li>
    <li>
      @guest
          <a class="mr-2" href="{{ route('login') }}">
            <i class="fas fa-user fa-2x"></i>
          </a>
      @else
        <a id="navYate" class="ml-3 dropdown-toggle noLetterSpace" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navYate">
            <a class="noLetterSpace noLetterSpace-user-menu dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

            <a class="noLetterSpace noLetterSpace-user-menu dropdown-item" href="{{ url('/user/home') }}">
                {{ __('Mi zona') }}
            </a>
        </div>
      @endguest
    </li>
  </ul>
  <i class="fas fa-bars fa-2x" id="burger"></i>
</nav>
<div id="goUp">
  <i id="goUpB" class="fas fa-arrow-up fa-2x"></i>
</div>
