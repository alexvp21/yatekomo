<footer>
    <div class="container-fluid">
        <div class="row mt-5">
            <div class="col-12 foot-container d-flex justify-content-center">
                <div class="col foot-nav">
                    <ul class="foot-links">
                        <div>
                            <li>
                                <a href="/"><h5>Inicio</h5></a>
                            </li>
                            <li>
                                <a href="/barcos"><h5>Barcos</h5></a>
                            </li>
                            <li>
                                <a href="/quienesomos"><h5>Quienes somos</h5></a>
                            </li>
                            <li>
                                <a href="/contacto"><h5>Contactanos</h5></a>
                            </li>
                        </div>
                    </ul>
                </div>
                <div class="col foot-nav">
                    <div class="media-footer">
                        <div class="text-center">
                            <h5>Síguenos en nuestras redes sociales:</h5>
                        </div>
                        <div class="footiconos">
                            <i class="fab fa-linkedin"></i>
                            <i class="fab fa-twitter-square"></i>
                            <i class="fab fa-instagram"></i>
                            <i class="fab fa-facebook-square"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
