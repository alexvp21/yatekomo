<head>
    <link rel="stylesheet" href="css/quiensomos.css">
    <script src="js/quiensomos.js"></script>
</head>
@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row d-flex justify-content-center">
        <div id="carousel-banner" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="img"><img class="img1" src="img/img-quienesSomos-1.png" alt="Primer slide"></div>
                </div>
                <div class="carousel-item">
                    <div class="img"><img class="img1" src="img/img-quienesSomos-2.png" alt="Segundo slide"></div>
                </div>
                <div class="carousel-item">
                    <div class="img"><img class="img1" src="img/img-quienesSomos-3.png" alt="Tercer slide"></div>
                </div>
                <div class="carousel-item">
                    <div class="img"><img class="img1" src="img/img-quienesSomos-4.png" alt="Cuarto slide"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container contenedor mt-5 mb-5">
    <h1>Quienes somos</h1>
    <p class="lead">
    Somos la empresa de alquiler de yates y embarcaciones de moda en España .
    Llevamos más de 10 años de experiencia en ofrecer la mejor flota de alquiler de yates,
    veleros y motoras para un público exigente, amante de la navegación,
    del mar y de la práctica de la náutica en el Mar Mediterráneo..
    </p>
</div>
<div class="container contenedor mt-5 mb-5">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm col-xs-12">
            <img class="imgPerfil img-thumbnail" src="img/adry_capi.jpg">
            <div class="card-body descripcion">
                <p class="card-text">Adria Grive</p>
                <small class="text-muted">Community Manager</small>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm  col-xs-12">
            <img class="imgPerfil img-thumbnail" src="img/alex_capi.jpg">
            <div class="card-body descripcion">
                <p class="card-text">Alexander Vargas</p>
                <small class="text-muted">Director General</small>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm col-xs-12">
            <img class="imgPerfil imgPerfil1 img-thumbnail mx-auto" src="img/edu_capi.jpg">
            <div class="card-body descripcion">
                <p class="card-text">Eduard Rafegas</p>
                <small class="text-muted">Responsable de flota</small>
            </div>
        </div>
    </div>
</div>
<section class="jumbotron text-center">
    <div class="container contenedor">
        <h1> Una amplia flota de calidad y una atención personalizada </h1>
        <p class="lead contenedor">
            En "UNYATEENELMAR" no actuamos como agencia, somos una empresa con un amplio equipo de profesionales dedicados al 
            mantenimiento y alquiler de una flota propia de veleros,yates y motoras, de calidad y siempre actualizada,
            comprometidos en ofrecer el mejor servicio y atención personalizada a nuestros clientes.
        </p>
        <h1 class="mt-4"> Compartimos tu pasión por el mar.</h1>
        <p class="lead contenedor mb-0">
        En "UNYATEENELMAR", empresa de alquiler de yates y otras embarcaciones en el mar mediterráneo, 
        compartimos tu pasión por el mar y la náutica. Por esto nos gusta cuidar de nuestras embarcaciones
        y también de tu bienestar durante el periodo de alquiler. Siempre estamos pendientes de que disfrutes
        de una excelente experiencia de navegación. Nuestro equipo te ofrecerá servicio desde el momento de 
        la elección de tu barco de alquiler, durante todo el periodo de navegación y en el momento de la
        entrega de la embarcación.
        </p>
    </div>
</section>
<div class="container mt-5 mb-5">
    <div class="row align-items-center">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs">    
                <img class="imgPerfil rounded border border-primary"  src="img/img-quienesSomos-5.jpg">
                <div class="card-body descripcion">
                    <p class="card-text">Xavier</p>
                    <small class="text-muted">Equipo de flota</small>
                </div>    
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs">    
                <img class="imgPerfil rounded border border-primary" src="img/img-quienesSomos-5.jpg">
                <div class="card-body descripcion">
                    <p class="card-text">Jaume</p>
                    <small class="text-muted">Responsable de reservas</small>
                </div>    
        </div>  <div class="col-lg-3 col-md-3 col-sm-6 col-xs">    
                <img class="imgPerfil rounded border border-primary" src="img/img-quienesSomos-5.jpg">
                <div class="card-body descripcion">
                    <p class="card-text">Carles</p>
                    <small class="text-muted">Patrón</small>
                </div>    
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs">    
                <img class="imgPerfil rounded border border-primary" src="img/img-quienesSomos-5.jpg">
                <div class="card-body descripcion">
                    <p class="card-text">Pedro</p>
                    <small class="text-muted">Patrón</small>
                </div>    
        </div>
    </div>
</div>

@endsection