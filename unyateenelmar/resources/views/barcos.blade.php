<head>
    <link rel="stylesheet" href="css/barcos.css">
    <!-- Add icon library -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
</head>
@extends('layouts.master')
@section('content')
<script src="js/barcos.js"></script>
<!-- Imagen header -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12 containerBanner">
            <div id="barcos-banner"></div>
        </div>
    </div>
</div>

<div class="container">
    <!-- Menu version movil -->
    <div class="dropdown" id="barcos-dropDownIconos">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false" data-placement="left" title="Tipos de barcos">
            <i class="fas fa-ellipsis-v"></i>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" id="barcosTodos" href="#">Todos los barcos</a>
            <a class="dropdown-item" id="barcosYates" href="#">Yates</a>
            <a class="dropdown-item" id="barcosVelero" href="#">Velero</a>
            <a class="dropdown-item" id="barcosGoletas" href="#">Goletas</a>
            <a class="dropdown-item" id="barcosCatamaranes" href="#">Catamaranes</a>
        </div>
    </div>
    <!-- Iconos seleccion versión PC -->
    <div class="row mb-5 controls" id="iconosBarcos">
        <div class="col-2 text-center hvr-underline-from-center" id="barcos-yateIcono">
            <div class="barcos-capa-icono filtr" data-cat="yate"></div>
            <img src="/img/barcos-img-2.png" alt="Yates icono" class="img-thumbnail">
            <h3>Yates</h3>
        </div>
        <div class="col-2 text-center hvr-underline-from-center" id="barcos-veleroIcono">
            <div class="barcos-capa-icono filtr" data-cat="velero"></div>
            <img src="/img/barcos-img-3.png" alt="Velero icono" class="img-thumbnail">
            <h3>Velero</h3>
        </div>
        <div class="col-4 text-center hvr-underline-from-center" id="barcos-todosIcono">
            <div class="barcos-capa-icono filtr" data-cat="all"></div>
            <img src="/img/barcos-img-6.png" alt="Todos icono" class="img-thumbnail">
            <h3>Todos los barcos</h3>
        </div>
        <div class="col-2 text-center hvr-underline-from-center" id="barcos-goletasIcono">
            <div class="barcos-capa-icono filtr" data-cat="goletas"></div>
            <img src="/img/barcos-img-4.png" alt="Goletas icono" class="img-thumbnail">
            <h3>Goletas</h3>
        </div>
        <div class="col-2 text-center hvr-underline-from-center" id="barcos-catamaranesIcono">
            <div class="barcos-capa-icono filtr" data-cat="catamaranes"></div>
            <img src="/img/barcos-img-5.png" alt="Catamaranes icono" class="img-thumbnail">
            <h3>Catamaranes</h3>
        </div>
    </div>
    <!-- Cartas de los barcos -->
    <div class="row">
        <div class="col-12 col-md-12 title">
            <button class="btn btn-success btn-collapse" id="collapseBtn" type="button" data-toggle="collapse" data-target="#advancedSearch" aria-expanded="false" aria-controls="advancedSearch">
                <i class="far fa-plus-square"></i> Busqueda avanzada
            </button>
            <form method="post" action="{{ url('/advancedSearch') }}" class="collapse" id="advancedSearch">
                @csrf
                <div class="cards">
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Tipo</p>
                            <select class="form-control" name="tipo" title="tipo de yate">
                                <option value="todo">Todos</option>
                                <option value="yate">Yate</option>
                                <option value="velero">Velero</option>
                                <option value="catamaran">Catamaran</option>
                                <option value="goleta">Goleta</option>
                            </select>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Eslora</p>
                            <select class="form-control" name="eslora">
                                <option value="50">50m</option>
                                <option value="100">100m</option>
                                <option value="150">150m</option>
                                <option value="200">200m</option>
                            </select>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Pasajeros</p>
                            <input type="number" placeholder="Nº pasajeros" class="form-control" name="pasajero_max"
                                id="pasajeros" title="mínimo 2 pax" min="2" max="10" required />
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Año desde</p>
                            <select class="form-control" name="anyo">
                                @foreach($anyos as $id => $anyo)
                                    <option value="{{$anyo}}">{{$anyo}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Camarotes</p>
                            <select class="form-control" name="camarote">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Fecha Inicio</p>
                            <div class="input-group-append">
                                <input type="text" name="fecha_inicio" readonly class="form-control date calPicker mr-3" id="calPickerA"
                                    placeholder="Selecciona una fecha" title="Escoge la fecha" required />
                                <i class="fas fa-calendar-day fa-2x" id="btnPickerA"></i>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <p class="card-text">Precio desde</p>
                            <select class="form-control" name="precio">
                                <option value="0">Cualquier precio</option>
                                <option value="100">100€</option>
                                <option value="200">200€</option>
                                <option value="300">300€</option>
                                <option value="400">400€</option>
                                <option value="500">500€</option>
                            </select>
                        </div>
                    </div>
                </div>
                <button id="buscaBarco" data-target="advancedSearch" class="btn btn-success welcome-search" title="Busca tu yate">Buscar!</button>
            </form>
        </div>
    </div>
    @if (session('statusFail'))
        <div class="alert alert-danger">
            {{ session('statusFail') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="row mt-4">
        <!-- Zona de cartas de barco -->
        @isset($barcos)
            @for ($i=0; $i <sizeof($barcos); $i++)
                <div class="col-12 col-md-4 text-center mt-3 filtr-elem" data-filter="{{$barcos[$i]['tipo']}}">
                    <form class="card text-white bg-light barcos-cartas" action="{{ url('/reservar/') }}" method="POST">
                    @csrf
                    <input type="hidden" name="fecha_inicio" class="fecha-ini"/>
                    <input type="hidden" name="barco_id" value="{{ $barcos[$i]['barco_id'] }}" />
                        <img src="{{$barcos[$i]['imagen_portada']}}" class="img-responsive" alt="Test Yate">
                        <div class="card-body">
                            <h5 class="card-title">{{$barcos[$i]['nombre_barco']}}</h5>
                            <div class="pictos mt-3">
                                <i class="fas fa-user-friends" title="Pasajeros: {{$barcos[$i]['pasajero_max']}}"></i>
                                <i class="fas fa-door-open" title="Camarotes: {{$barcos[$i]['camarote']}}"></i>
                                <i class="fas fa-ship" title="Eslora: {{$barcos[$i]['eslora']}}m"></i>
                                <i class="fas fa-euro-sign" title="Precio/dia: {{$barcos[$i]['precio_dia']}}€"></i>
                                <i class="fas fa-bed" title="Camas: {{$barcos[$i]['plazas_cama']}}"></i>
                                <i class="fas fa-thumbs-up" title="Positividad: {{$barcos[$i]['voto_positivo']}}"></i>
                                <i class="fas fa-thumbs-down" title="Negatividad: {{$barcos[$i]['voto_negativo']}}"></i>
                            </div>
                            <button class="btn btn-success mt-3 reservar">
                                {{ __('Reservar') }}
                            </button>
                        </div>
                    </form>
                </div>
            @endfor
            @if(sizeof($barcos) == 0)
                <div class="alert alert-danger" role="alert">
                    <p>La búsqueda hizo aguas, intenta de nuevo!</p>
                    <a class="link" href="/barcos">Quitar todos los filtros</a>
                </div>
            @endif
        @endisset
    </div>
</div>
@endsection