<head>
    <link rel="stylesheet" href="css/contactanos.css">
</head>
@extends('layouts.master')
@section('content')
<script src="js/contactanos.js"></script>
<div class="container-fluid containerBanner">
    <div class="col-12" id="contactanos-banner">

    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-12 text-justify text-center" id="tituloContactanos">
            <h1 style="text-align:center">¿Tienes alguna consulta sobre la contratación de algunos de nuestros servicios?</h1>
            <p>No dudes en rellenar el formulario que se encuentra a continuación, te enviaremos la respuesta en la mayor brevedad posible.
            </p>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-12 col-md-5 ">
            <h2 class="text-left">Contacte con nostros <i class="fas fa-info-circle"></i></h2>
            <div class="col-12 col-md-12 mx-auto" id="contactanos-formulario">
                <!-- Formulario contactanos -->
                <form action="https://formspree.io/agrive.ie@gmail.com" method="POST">
                    <div class="form-group" id="barcos-formulario-first-child">
                        <label for="inputNombre">Nombre</label>
                        <input type="text" name="nobmre" class="form-control" id="inputNombre" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <label for="inputApellidos">Apellidos</label>
                        <input type="text" class="form-control" id="inputApellidos" name="apellidos" placeholder="Apellidos">
                    </div>
                    <div class="form-group">
                        <label for="inputTelefono">Telefono</label>
                        <input type="number" class="form-control" name="inputTelefono" id="inputTelefono" placeholder="Teléfono">

                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Email</label>
                        <input type="email" class="form-control" name="_replayto" id="inputEmail" placeholder="email@ejemplo.com">

                    </div>
                    <div class="form-group">
                        <label for="inputBarco">Tipo de embarcación</label>
                        <select class="form-control" name="tipoEmbarcacion" id="inputBarco">
                            <option>Todos</option>
                            <option>Yates</option>
                            <option>Veleros</option>
                            <option>Goletas</option>
                            <option>Catamaranes</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Cuentanos</label>
                        <textarea class="form-control" name="Mensaje" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" name="acceptaTerminos" class="form-check-input" id="checkTerminos">
                        <label class="form-check-label" for="checkTerminos" required>Accepto los terminos de
                            condiciones</label>
                    </div>
                    <button type="submit" class="btn" id="btnFormularioContactanos">Enviar</button>
                </form>
            </div>

        </div>
        <div class="col-12 col-md-7">
            <h2>Donde estamos?</h2>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2989.2974342275784!2d2.3118253148001373!3d41.47615099845182!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4b097daaf73ed%3A0x119e2036f3a6aaba!2sMarina+Estrella%2C+S.L.!5e0!3m2!1sca!2ses!4v1556302499900!5m2!1sca!2ses" width="100%" height="50%" frameborder="0" style="border:0" allowfullscreen>
            </iframe>
        </div>
    </div>

</div>
@endsection