<!--
    Reserva:
    escoger barco
    escoger ruta
    si quiere o no menu
    alegernos
    precio final / tasas
    pagar (tarjeta / paypal)
-->

<head>
    <link rel="stylesheet" href="../css/reserva.css">
</head>
@extends('layouts.master')
@section('content')
<script src="../js/reserva.js"></script>
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            <div class="eleccion is-active">
                <h3 class="title mt-2"><b>Tu elección:</b></h3>
                <div class="eleccion-content text-center pt-3">
                    <img src="{{ $barco['imagen_portada'] }}" alt="barco">
                    <div class="grid">
                        <h5><strong>Eslora:</strong> {{ $barco['eslora'] }}m</h5>
                        <h5><strong>Camarotes:</strong> {{ $barco['camarote'] }}</h5>
                        <h5><strong>Tipo:</strong> {{ $barco['tipo'] }}</h5>
                        <h5><strong>Pasajeros:</strong> {{ $barco['pasajero_max'] }}</h5>
                        <h5><strong>Año:</strong> {{ $barco['anyo'] }}</h5>
                        <h5><strong>Precio/dia:</strong> {{ $barco['precio_dia'] }}€</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt-5">
    <form id="reservaForm" method="post" action="{{ url('/reservarBarco') }}">
        @csrf
        <div class="form-seleccion">
            <h3>Trayectos</h3>
            <div class="form-group selects">
                @foreach($trayectos as $trayecto)
                <div class="card card-select trayecto" data-type="trayecto" 
                data-select="{{ 'trayecto_'.$trayecto->trayecto_id }}" precio="{{ $trayecto->precioTrayecto }}">
                    <img src="{{ $trayecto->url_Imagen }}" class="card-img-top" alt="trayecto">
                    <div class="card-body">
                        <h5 class="card-title">
                            <input type="radio" value="{{ $trayecto->trayecto_id }}" class="select-radio" name="trayecto" id="{{ 'trayecto_'.$trayecto->trayecto_id }}" />
                            <label for="{{ 'trayecto_'.$trayecto->trayecto_id }}">{{ $trayecto->nombre_trayecto }}</label>
                        </h5>
                        <h5>{{ $trayecto->precioTrayecto }}€</h5>
                        <h2>{{ $trayecto->horas }} h.</h2>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="form-seleccion mt-5">
            <h3>Menús</h3>
            <div class="form-group selects">
                @foreach($menus as $menu)
                <div class="card card-select menu" data-type="menu" data-select="{{ 'menu_'.$menu->menu_id }}" precio="{{ $menu->precioPorPersona }}">
                    <div class="card-body">
                        <h5 class="card-title">
                            <input type="radio" value="{{ $menu->menu_id }}" class="select-radio" name="menu" id="{{ 'menu_'.$menu->menu_id }}" />
                            <label for="{{ 'menu_'.$menu->menu_id }}">{{ $menu->nombre_menu }}</label>
                        </h5>
                        <h5>{{ $menu->precioPorPersona }}€</h5>
                        <h5>{{ $menu->alergenos }}</h5>
                        <p>Primeros: {{ $menu->primerosMenu }}</p>
                        <p>Segundos: {{ $menu->segundosMenu }}</p>
                        <p>Postres: {{ $menu->postresMenu }}</p>
                        <p>Bebidas: {{ $menu->bebidasMenu }}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="form-seleccion mt-5" id="pasajeros">
            <div class="eleccion-content-pass">
                <h3>Pasajeros</h3>
                <a class="ml-3 btn btn-success accBtn"><i class="far fa-plus-square"></i></a>
            </div>
        </div>
        <div class="form-seleccion mt-5">
            <h3>Información de pago</h3>
            <div class="selects mt-3">
                <label for="nombre_titular">Nombre del titular:
                    <input class="form-control" type="text" id="nombre_titular" name="nombre_titular"/>
                </label>
                <label for="num_tarjeta">Número de tarjeta:
                    <input class="form-control" type="text" id="num_tarjeta" name="num_tarjeta"/>
                </label>
                <label for="cod_seguridad">CVC:
                    <input class="form-control" type="text" id="cod_seguridad" name="cod_seguridad" />
                </label>
                <label for="fecha_caducidad">Caducidad:
                    <input class="form-control" type="text" id="fecha_caducidad" name="fecha_caducidad"/>
                </label>
            </div>
        </div>
        <div class="form-seleccion mt-5">
            <div class="eleccion-content-pass">
                <h3 class="title"><b>Total a pagar:</b></h3>
                <h2 class="ml-3" id="precio_final"></h2>
            </div>
            <button type="submit" id="pagar" class="btn btn-success">Pagar</button>
        </div>
        <input type="hidden" id="contador" name="contador"/>
        <input type="hidden" name="fecha_inicio" value="{{ $fechaInicio }}"/>
        <input type="hidden" name="precio_total" id="precioTotal"/>
        <input type="hidden" name="barco" value="{{ $barco['barco_id'] }}"/>
        <input type="hidden" name="fechaCompra" id="fecha_compra"/>
        <input type="hidden" name="capitan" value="{{ $capitan }}"/>
        <input type="hidden" name="chef" value="{{ $chef }}"/>
    </form>
    <input type="hidden" id="max_pasajeros" value="{{ $barco['pasajero_max'] }}" />
    <input type="hidden" id="precio_barco" value="{{ $barco['precio_dia'] }}" />
    <input type="hidden" value="0" id="precio_trayecto" />
    <input type="hidden" value="0" id="precio_menu" />
</div>
@endsection