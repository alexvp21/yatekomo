<?php

namespace App\Http\Middleware;

use Closure;
Use Auth;
use Illuminate\Support\Facades\DB;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $userId = Auth::user()->id;
        $cliente = DB::table('cliente')
        ->select('rol')
        ->where('usuario_id', '=', $userId)
        ->get();
        $clienteRol = json_decode(json_encode($cliente), true)[0]['rol'];
        
        if ($clienteRol != 'administrador') {
            return redirect('/');
        }

        return $response;
    }
}
