<?php
/*
AdminController 
  Se encarga de procesar todos los datos recibidos de las vistas de admin y retornara esos datos a las vistas una vez los metodos hayan finalizado su proceso. 

  Vistas Admin
  ------------
  barcos.blade
  incidencias.blade
  reservas.blade
  usuarios.blade
  valoraciones.blades

*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;

class adminController extends Controller
{
    //getIndexView: Recupera los datos de la tabla barco y los devuelve a la vista admin.barcos en formato json
    function getIndexView(){
      $barcos = DB::table('barco')
      ->get();
      $barcos = json_decode(json_encode($barcos), true);

     $anyos = DB::table('anyos')->pluck('anyo');
     $anyos = json_decode(json_encode($anyos), true);
      
      return view('admin.barcos', [
        'barcos' => $barcos,
        'anyos' => $anyos,
      ]); 
    }

    //getUsuariosView: Recupera los datos de la tabla users y los devuelve a la vista admin.usuarios en formato json
    function getUsuariosView(){
      $usuarios = DB::table('users')
      ->join('cliente', 'users.id', '=', 'cliente.usuario_id')
      ->get();
      $usuarios = json_decode(json_encode($usuarios), true);   
      return view('admin.usuarios', ['usuarios' => $usuarios]); 
    }

    //getValoracionView: Recupera los datos de la tabla valoracion y los devuelve a la vista admin.valoraciones en formato json
    function getValoracionView(){
      $valoraciones = DB::table('valoracion')
      ->join('barco', 'valoracion.barco_id', '=', 'barco.barco_id')
      ->join('users', 'valoracion.cliente_id', '=', 'users.id')
      ->join('reserva', 'valoracion.reserva_id', '=', 'reserva.reserva_id')
      ->get();
      $valoraciones = json_decode(json_encode($valoraciones), true);
      return view('admin.valoraciones', ['valoraciones' => $valoraciones]);
    }

    //getIncidenciasView: Recupera los datos de la tabla incidencia y los devuelve a la vista admin.incidencias en formato json
    function getIncidenciasView(){
      $incidencias = DB::table('incidencia')
      ->join('barco', 'incidencia.barco_id', '=', 'barco.barco_id')
      ->join('capitan', 'incidencia.capitan_id', '=', 'capitan.capitan_id')
      ->get();
      $incidencias = json_decode(json_encode($incidencias), true);
      return view('admin.incidencias', ['incidencias' => $incidencias]);
    }
    
    //getReservasView: Recupera los datos de la tabla reserva y los devuelve a la vista admin.reservas en formato json
    function getReservasView(){
      $reservas = DB::table('reserva')
      ->join('barco', 'reserva.barco_id', '=', 'barco.barco_id')
      ->join('capitan', 'reserva.capitan_id', '=', 'capitan.capitan_id')
      ->join('cliente', 'reserva.cliente_id', '=', 'cliente.cliente_id')
      ->join('users', 'reserva.cliente_id', '=', 'users.id')
      ->get();
      $reservas = json_decode(json_encode($reservas), true); 
      return view('admin.reservas', ['reservas' => $reservas]);
    }
    

    function barcoSpec($barcoId) {
      $barco = DB::table('barco')
      ->where('barco_id', '=', $barcoId)->get();
      $barco = json_encode($barco);
      echo $barco;
    }

    function CLienteSpec($clienteId) {
      $clienteId = DB::table('cliente')
      ->where('cliente_id', '=', $clienteId)->get();
      $clienteId = json_encode($clienteId);
      echo $clienteId;
    }

    function reservaSpec($reservaId) {
      $reserva = DB::table('reserva')
      ->where('reserva_id', '=', $reservaId)->get();
      $reserva = json_encode($reserva);
      echo $reserva;
    }


    function modifyCliente(Request $req) {
      $clienteInfo = $req->input();
      $clienteInsert = $req->except(['_token','cliente_id']);
      $valida = Validator::make($clienteInfo, [
            'rol' => ['string', 'max:25'],

          ]);
      
      if ($valida->fails()) {
          return redirect()->back()->with('statusError', 'Fallo en la actualización');
        } else {
          DB::table('cliente')
          ->where('cliente_id', '=', $clienteInfo['cliente_id'])
          ->update($clienteInsert);
          return redirect()->back()->with('statusOk', 'Actualizado correctamente');
        }
      }

    function modifyBarco(Request $req) {
          $barcoInfo = $req->input();
          $barcoInsert = $req->except(['_token', 'barco_id']);
          $valida = Validator::make($barcoInfo, [
            'nombre_barco' => ['string', 'max:255'],
            'eslora' => ['regex:/^\d+(\.\d{1,2})?$/'],
            'pasajero_max' => ['integer'],
            'precio_dia' => ['regex:/^\d+(\.\d{1,2})?$/'],
            'plazas_cama' => ['integer'],
          ]);
                     
          if ($valida->fails()) {
            return redirect()->back()->with('statusError', 'Fallo en la actualización');
          } else {
            DB::table('barco')
            ->where('barco_id', '=', $barcoInfo['barco_id'])
            ->update($barcoInsert);
            return redirect()->back()->with('statusOk', 'Actualizado correctamente');
          }
    }

    function modifyReserva(Request $req) {
      $reservaInfo= $req->input();
      $reservaInsert = $req->except(['_token','reserva_id']);
      $valida = Validator::make($reservaInfo, [
            'estado' => ['string', 'max:25'],

          ]);
      
      if ($valida->fails()) {
          return redirect()->back()->with('statusError', 'Fallo en la actualización');
        } else {
          DB::table('reserva')
          ->where('reserva_id', '=', $reservaInfo['reserva_id'])
          ->update($reservaInsert);
          return redirect()->back()->with('statusOk', 'Actualizado correctamente');
        }
      }


      /* FUNCIONES DELETE  */
    function borrarBarco($barcoId) {
      DB::beginTransaction();
      $incidenciaId = DB::table('incidencia')
      ->where('barco_id', '=', $barcoId)
      ->delete();

      $barcoDel = DB::table('barco')
      ->where('barco_id', '=', $barcoId)
      ->delete();
      DB::commit();
      return redirect()->back()->with('statusOk', 'Borrado correctamente');
    }

  

    function borrarUsuario($usuId) {

      DB::beginTransaction();
      $favoritoId = DB::table('favorito')
      ->where('cliente_id', '=', $usuId)
      ->delete();

      $clienteId = DB::table('users')
      ->where('id', '=', $usuId)
      ->delete();
      DB::commit();
      return redirect()->back()->with('statusOk', 'Borrado correctamente');
    }


    function borrarValoracion($valId) {

      DB::beginTransaction();
      $valoracionId = DB::table('valoracion')
      ->where('valoracion_id', '=', $valId)
      ->delete();
      DB::commit();
      return redirect()->back()->with('statusOk', 'Borrado correctamente');
    }


    function borrarIncidencia($incId) {

      DB::beginTransaction();
      $incidenciaId = DB::table('incidencia')
      ->where('incidencias_id', '=', $incId)
      ->delete();
      DB::commit();
      return redirect()->back()->with('statusOk', 'Borrado correctamente');
    }


    function borrarReserva($ResId) {

      DB::beginTransaction();
      $reservaId = DB::table('reserva')
      ->where('reserva_id', '=', $ResId)
      ->delete();
      DB::commit();
      return redirect()->back()->with('statusOk', 'Borrado correctamente');
    }

    function insertBarco(Request $req) {
      $insertAll = $req->input();
      $insert = $req->except('_token');
      $valida = Validator::make($insertAll, [
        'nombre_barco' => ['string'],
        'eslora' => ['regex:/^\d+(\.\d{1,2})?$/'],
        'pasajero_max' => ['integer'],
        'precio_dia' => ['regex:/^\d+(\.\d{1,2})?$/'],
        'plazas_cama' => ['integer'],
        'camarotes' => ['integer'],
        'anyo' => ['integer'],
      ]);

      if ($valida->fails()) {
        return redirect()->back()->with('statusError', 'Rellena todos los campos!');
      } else {
        $insert['fianza'] = 50.5;
        $insert['voto_negativo'] = 0;
        $insert['voto_positivo'] = 0;
        if ($req->hasFile('imagen_portada')) {
          $avatar = $req->file('imagen_portada');
          $filename = time() . '.' . $avatar->getClientOriginalExtension();
          Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/barcos/' . $filename));
          $insert['imagen_portada'] = '/uploads/barcos/'.$filename;
        }

        DB::table('barco')
        ->insert($insert);

        $anyos = DB::table('anyos')->pluck('anyo');
        $anyos = json_decode(json_encode($anyos), true);
      
        return redirect('/admin/barcos')->with('statusOk', 'Barco insertado!'); 
      }
    }

    function insertIncidencia(Request $req) {
      $insertAll = $req->input();
      $insert = $req->except('_token');
  
      $insert['hora_inicidencia'] = date('Y-m-d H:i:s');
     // echo ($insert['hora_inicidencia']);
     $valida = Validator::make($insertAll, [
     //'hora_inicidencia' => ['string'],
        'info'=> ['string'],
        'barco_id' => ['integer'],
        'capitan_id' => ['integer'],
      ]);

      if ($valida->fails()) {
        return redirect()->back()->with('statusError', 'Rellena todos los campos!');
      } else {
       $insert['hora_inicidencia'];
       $insert['info'];
        $insert['barco_id'];
        $insert['capitan_id'];
        }

        DB::table('incidencia')
        ->insert($insert);
        return redirect()->back()->with('statusOkey', 'Incidencia añadida correctamente!');
      }
}
