<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class transactionController extends Controller
{
    use RegistersUsers;

    public function registroCliente(Request $data)
    {
        $infoUser = $data->input();

        //Mensajes de validación
        $mensajes = array(
            'name.required' => 'El NOMBRE Y APELLIDOS es necesario',
            'telefono.required' => 'El TELEFONO es necesario',
            'sexo.required' => 'El SEXO es necesario',
            'dni.required' => 'El DNI es necesario',
            'fechaNacimiento.required' => 'La FECHA es necesaria',
            'email.required' => 'El EMAIL es necesario',
            'telefono.regex' => 'El FORMATO es incorrecto',

        );

        //Reglas de validación
        $reglas = [
            'telefono' => ['required', 'integer', 'min:12'],
            'sexo' => ['string'],
            'dni' => ['string'],
            'fechaNacimiento' => ['string', 'date'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];

        //Ejecutamos la validación
        $valida = Validator::make($infoUser, $reglas, $mensajes);

        if ($valida->fails()) {
            return redirect()
                ->back()
                ->withErrors( $valida )
                ->withInput();
        } else {
            $time = strtotime($infoUser['fecha_nacimiento']);
            $fechaNac = date('Y-m-d',$time);
            $id;
            DB::beginTransaction();
                $id = DB::table('users')->insertGetId([
                    'name' => $infoUser['name'],
                    'email' => $infoUser['email'],
                    'password' => Hash::make($infoUser['password']),
                ]);
                DB::table('cliente')->insert([
                    'fechaNacimiento' => $fechaNac,
                    'sexo' => $infoUser['sexo'],
                    'dni' => $infoUser['dni'],
                    'telefono' => $infoUser['telefono'],
                    'puntos' => 0,
                    'rol' => 'usuario',
                    'usuario_id' =>$id
                ]);
            DB::commit();
            Auth::loginUsingId($id, TRUE);
            return redirect('/user/home');
        }
    }
}
