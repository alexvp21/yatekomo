<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class mainController extends Controller
{
    function getIndexView() {
        return view('welcome');
    }

    function getBarcosView() {
        //Recuperamos todos los barcos
        $barcos = DB::table('barco')
        ->get();
        $barcos = json_decode(json_encode($barcos), true); //Lo convertimos en array
        
        $anyos = DB::table('anyos')->pluck('anyo');
        $anyos = json_decode(json_encode($anyos), true);
        return view('barcos', array(
            'barcos' => $barcos,
            'anyos' => $anyos
        ));
    }

    function getContactView() {
        return view('contactanos');
    }

    function getQuienView() {
        return view('quiensomos');
    }

    function inicioSearch(Request $req) {
        $input = $req->except('_token');

        $input = $req->input();

        $reglas = [
            'fecha_inicio' =>
            ['required'],
            'eslora' =>
            ['required'],
            'pasajero_max' =>
            ['required'],
            'tipo' =>
            ['required'],
        ];

        $validacion = Validator::make($input, $reglas);

        if ($validacion->fails()) {
            return redirect('/')->with('statusFail', 'Ups! faltan campos por rellenar.');
        } else {
            $barcosFilter = DB::table('barco')
            ->join('reserva', 'barco.barco_id', '=', 'reserva.barco_id')
            ->whereRaw(
                "(fecha_inicio = '".$input['fecha_inicio']."')"
            )
            ->pluck('barco.barco_id');

            $barcosFilter = json_decode(json_encode($barcosFilter), true);

            $barcos; 
            if($input['tipo'] != 'todo') {
                $barcos = DB::table('barco')
                ->where(array(
                    array('tipo', '=', $input['tipo']),
                    array('eslora', '<=', floatval($input['eslora'])),
                    array('pasajero_max', '>=', $input['pasajero_max']),
                ))
                ->whereNotIn('barco.barco_id', $barcosFilter)
                ->get();
            } else {
                $barcos = DB::table('barco')
                ->where(array(
                    array('eslora', '<=', floatval($input['eslora'])),
                    array('pasajero_max', '>=', $input['pasajero_max']),
                ))
                ->whereNotIn('barco.barco_id', $barcosFilter)
                ->get();
            }

            $barcos = json_decode(json_encode($barcos), true);

            $anyos = DB::table('anyos')->pluck('anyo');
            $anyos = json_decode(json_encode($anyos), true);
            return view('barcos', array(
                'barcos' => $barcos,
                'anyos' => $anyos
            ));
        }
    }

    // ----- BARCOS ------ //

    function barcoSearch(Request $req) {
        $input = $req->except('_token');
        $input = $req->input();

        $reglas = [
            'fecha_inicio' =>
            ['required'],
            'eslora' =>
            ['required'],
            'tipo' =>
            ['required'],
            'pasajero_max' =>
            ['required'],
            'anyo' =>
            ['required'],
            'camarote' =>
            ['required'],
            'precio' =>
            ['required'],
        ];

        $validacion = Validator::make($input, $reglas);

        if ($validacion->fails()) {
            return redirect('/barcos')->with('statusFail', 'Ups! Hubo un error inesperado.');
        } else {
            $barcosFilter = DB::table('barco')
            ->join('reserva', 'barco.barco_id', '=', 'reserva.barco_id')
            ->whereRaw(
                "(fecha_inicio = '".$input['fecha_inicio']."')"
            )
            ->pluck('barco.barco_id');

            $barcosFilter = json_decode(json_encode($barcosFilter), true);

            $barcos; 
            if($input['tipo'] != 'todo') {
                $barcos = DB::table('barco')
                ->where(array(
                    array('tipo', '=', $input['tipo']),
                    array('eslora', '<=', floatval($input['eslora'])),
                    array('pasajero_max', '>=', $input['pasajero_max']),
                    array('anyo', '>=', $input['anyo']),
                    array('camarote', '>=', $input['camarote']),
                    array('precio_dia', '>=', $input['precio']),
                ))
                ->whereNotIn('barco.barco_id', $barcosFilter)
                ->get();
            } else {
                $barcos = DB::table('barco')
                ->where(array(
                    array('eslora', '<=', floatval($input['eslora'])),
                    array('pasajero_max', '>=', $input['pasajero_max']),
                    array('anyo', '>=', $input['anyo']),
                    array('camarote', '>=', $input['camarote']),
                    array('precio_dia', '>=', $input['precio']),
                ))
                ->whereNotIn('barco.barco_id', $barcosFilter)
                ->get();
            }

            $barcos = json_decode(json_encode($barcos), true);

            $anyos = DB::table('anyos')->pluck('anyo');
            $anyos = json_decode(json_encode($anyos), true);
            return view('barcos', array(
                'barcos' => $barcos,
                'anyos' => $anyos
            ));
        }
    }
    // ----- FIN BARCOS -- //
    
    function reservarBarco(Request $req) {
        $input = $req->input();

        $reglas = [
            'barco_id' =>
            ['required'],
            'fecha_inicio' => 
            ['required'],
        ];

        $validacion = Validator::make($input, $reglas);

        if ($validacion->fails()) {
            return redirect('/barcos')->with('statusFail', 'Ups! Hubo un error inesperado.');
        } else {
            $barcoId = $input['barco_id'];
    
            $barco = DB::table('barco')
            ->where('barco_id', '=', $barcoId)
            ->get();

            $capitan = DB::table('capitan')
            ->join('reserva', 'capitan.capitan_id', '=', 'reserva.capitan_id')
            ->where('reserva.fecha_inicio', '<>', $input['fecha_inicio'])
            ->first();

            if ($capitan == null) {
                return redirect('/barcos')->with('statusFail', 'Ups! No hay capitanes disponibles.');
            }

            $chef = DB::table('chef')
            ->join('reserva', 'chef.chef_id', '=', 'reserva.chef_id')
            ->where('reserva.fecha_inicio', '<>', $input['fecha_inicio'])
            ->first();

            if ($chef == null) {
                return redirect('/barcos')->with('statusFail', 'Ups! No hay chefs disponibles.');
            }

            $trayectos = DB::table('trayecto')
            ->get();

            $menus = DB::table('menu')
            ->get();
    
            $barco = json_decode(json_encode($barco), true);
            return view('reserva', array(
                'barco' => $barco[0],
                'trayectos' => $trayectos,
                'menus' => $menus,
                'fechaInicio' => $input['fecha_inicio'],
                'capitan' => $capitan->capitan_id,
                'chef' => $chef->chef_id,
            ));
        }

    }

    function reservar(Request $req) {
        // Constantes para posteriormente ingresar pasajeros
        $NOMBRE = 'nombre';
        $APELLIDO = 'apellidos';
        $DNI = 'dni';
        $FECHANACIMIENTO = 'fechaNacimiento';

        $input = $req->all();
        $contador = ($input['contador'] - 1);

        // get User id
        $userId = Auth::user()->id;
        $cliente = DB::table('cliente')
        ->select('cliente_id')
        ->where('usuario_id', '=', $userId)
        ->get();
        $clienteId = json_decode(json_encode($cliente), true)[0]['cliente_id'];

        DB::beginTransaction();
            if ($contador == 0) {

                // array para insertar posteriormente
                $arrayPasajero = array();
                for ($i=1; $i <= $contador; $i++) { 
                    // array temporal de pasajeros
                    $tmpArray = array(
                        $NOMBRE => $input[$NOMBRE.$i],
                        $APELLIDO => $input[$APELLIDO.$i],
                        $FECHANACIMIENTO => $input[$FECHANACIMIENTO.$i],
                        $DNI => $input[$DNI.$i],
                    );
                    array_push($arrayPasajero, $tmpArray);
                }

                DB::table('pasajeros')->insert($arrayPasajero);
            }

            $arrayfactura = array(
                'forma_de_pago' => 'tarjeta',
                'precio' => $input['precio_total'],
                'detalles' => '',
                'fecha_de_compra' => $input['fechaCompra']
            );

            $facturaId = DB::table('factura')->insertGetId($arrayfactura);
            
            // el array para insertar la reserva
            $arrayReserva = array(
                'fecha_inicio' => $input['fecha_inicio'],
                'precioTotal' => $input['precio_total'],
                'estado' => 'pendiente',
                'numPasajeros' => $contador,
                'trayecto_id' => $input['trayecto'],
                'barco_id' => $input['barco'],
                'chef_id' => $input['chef'],
                'cliente_id' => $clienteId,
                'factura_id' => $facturaId,
                'menu_id' => $input['menu'],
                'capitan_id' => $input['capitan'],
            );

            DB::table('reserva')->insert($arrayReserva);
        DB::commit();
            return redirect('/')->with('statusOk', 'Barco reservado!');
        
    }

}
