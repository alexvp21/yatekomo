<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use Intervention\Image\ImageManagerStatic as Image;
//use Illuminate\Foundation\Validation\ValidatesRequests;
// use Illuminate\Support\Facades\Validator;
// use Validator;


class userController extends Controller
{

    function getRol()
    {
        $admin = false;
        $userId = Auth::user()->id;
        $cliente = DB::table('cliente')
            ->select('rol')
            ->where('usuario_id', '=', $userId)
            ->get();
        $clienteRol = json_decode(json_encode($cliente), true)[0]['rol'];

        if ($clienteRol == 'administrador') {
            $admin = true;
        } else {
            $admin = false;
        }

        return $admin;
    }

    function getIndexView()
    {

        $id = Auth::user()->id;

        $user = DB::table('cliente')
            ->join('users', 'cliente.usuario_id', '=', 'users.id')
            ->where('users.id', '=', $id)
            ->get();

        //$user = Auth::user();
        $admin = $this->getRol();
        // print_r($user);
        return view('user.home', array(
            'user' => $user[0],
            'admin' => $admin,
        ));
    }

    //Vista favoritos
    function getFavView()
    {
        $admin = $this->getRol();
        //Obtenemos la id de usuario
        $id = Auth::user()->id;

        //Recuperamos los barcos favoritos
        $barcos = DB::table('favorito')
            ->join('cliente', 'favorito.cliente_id', '=', 'cliente.cliente_id')
            ->join('barco', 'favorito.barco_id', '=', 'barco.barco_id')
            ->join('users', 'cliente.usuario_id', '=', 'users.id')
            ->where('users.id', '=', $id) //falta id de session del usuario
            ->get();
        $barcos = json_decode(json_encode($barcos), true); //Lo convertimos en array
        // print_r($barcos);
        return view('user.favoritos', [
            'barcos' => $barcos,
            'admin' => $admin,
        ]);
        //    return redirect('/favoritos')
    }

    //Vista mis pagos
    function getPagosView()
    {
        $admin = $this->getRol();
        //Obtenemos la id de usuario
        $id = Auth::user()->id;
        //Consultamos todos los datos de pagos

        $facturas = DB::table('factura')
            ->join('reserva', 'factura.factura_id', '=', 'reserva.factura_id')
            ->join('cliente', 'reserva.cliente_id', '=', 'cliente.cliente_id')
            ->join('users', 'cliente.usuario_id', '=', 'users.id')
            ->join('barco', 'reserva.barco_id', '=', 'barco.barco_id')
            ->join('trayecto', 'reserva.trayecto_id', '=', 'trayecto.trayecto_id')
            ->join('menu', 'reserva.menu_id', '=', 'menu.menu_id')
            ->where('users.id', '=', $id)
            ->get();

        $facturas = json_decode(json_encode($facturas), true);
        // print_r($facturas);
        return view('user.mispagos', [
            'facturas' => $facturas,
            'admin' => $admin,
        ]);
    }

    //Vista mis reservas
    function getReservasView()
    {
        $admin = $this->getRol();
        //Obtenemos la id de usuario
        $id = Auth::user()->id;
        //Consultamos todos los datos de pagos
        $reservas = DB::table('reserva')
            ->join('factura', 'factura.factura_id', '=', 'reserva.factura_id')
            ->join('cliente', 'reserva.cliente_id', '=', 'cliente.cliente_id')
            ->join('users', 'cliente.usuario_id', '=', 'users.id')
            ->join('trayecto', 'reserva.trayecto_id', '=', 'trayecto.trayecto_id')
            // ->join('pasajeros', 'reserva.reserva_id', '=', 'pasajeros.reserva_id') // Peta con pasajeros
            ->join('menu', 'reserva.menu_id', '=', 'menu.menu_id')
            ->join('capitan', 'reserva.capitan_id', '=', 'capitan.capitan_id')
            ->join('chef', 'reserva.chef_id', '=', 'chef.chef_id')
            ->join('barco', 'reserva.barco_id', '=', 'barco.barco_id')
            ->where('users.id', '=', $id) //Hay que ponerle id
            ->get();


        $reservas = json_decode(json_encode($reservas), true);
        // print_r($reservas);
        return view('user.misreservas', [
            'reservas' => $reservas,
            'admin' => $admin,
        ]);
    }

    //Vista mis valoraciones
    function getValoracionView()
    {
        $admin = $this->getRol();
        //Obtenemos la id de usuario
        $id = Auth::user()->id;
        //Consultamos todos los datos de pagos
        $valoraciones = DB::table('valoracion')
            ->join('cliente', 'valoracion.cliente_id', '=', 'cliente.cliente_id')
            ->join('barco', 'valoracion.barco_id', '=', 'barco.barco_id')
            ->join('users', 'cliente.usuario_id', '=', 'users.id')
            ->where('users.id', '=', $id)
            ->get();

        $valoraciones = json_decode(json_encode($valoraciones), true);
        // print_r($valoraciones);
        return view('user.misvaloraciones', [
            'valoraciones' => $valoraciones,
            'admin' => $admin,
        ]);
    }

    public function actualizarDatos(Request $data)
    {
        $id = Auth::user()->id;

        //Datos
        $info_todo = $data->input();
        $info_cliente = $data->except(['_token', 'name', 'password', 'email']);
        $info_user = $data->except(['_token', 'fechaNacimiento', 'sexo', 'dni', 'telefono', 'puntos', 'rol']);

        //Mensajes de validación
        $mensajes = array(
            'name.required' => 'El NOMBRE Y APELLIDOS es necesario',
            'telefono.required' => 'El TELEFONO es necesario',
            'sexo.required' => 'El SEXO es necesario',
            'fechaNacimiento.date' => 'La FECHA no esta en formato correcto',
            'email.email' => 'El EMAIL no esta en formato correcto',
            'telefono.regex' => 'El FORMATO es incorrecto',
        );

        //Reglas de validación
        $reglas = [
            'name' =>
            ['required', 'string', 'max:255'],
            'telefono' =>
            ['required', 'integer', 'min:9'],
            'sexo' =>
            ['string'],
            'fechaNacimiento' =>
            ['date'],
            'email' =>
            ['email'],
        ];

        //Ejecutamos la validación
        $validacion = Validator::make($info_todo, $reglas, $mensajes);
        //Si falla mostramos errores y redireccionamos atras
        if ($validacion->fails()) {
            return redirect()
                ->back()
                ->withErrors($validacion)
                ->withInput()
                ->with('statusFail', 'Hubo un problema');
        } else {


            // $info_user=$data->input();
            // print_r($info_user);

            // enable the query log
            DB::enableQueryLog();
            DB::beginTransaction(); //Inicio transaccion
            $queryUser = DB::table('users')
                ->where('id', '=', $id)
                ->update(
                    $info_user
                );

            $query = DB::table('cliente')
                ->where('usuario_id', '=', $id)
                ->update(
                    $info_cliente
                );

            DB::commit(); //Fin transaccion
            // view the query log
            //  dd(DB::getQueryLog());
            return redirect()
                ->back()
                ->withInput()
                ->with('statusOk', 'Actualizado correctamente');
        }
    }

    function resetPassword(Request $request)
    {

        //Obtenemos la id de usuario
        $id = Auth::user()->id;
        // echo ($id);
        $currentuser = User::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'oldpass' => 'required|min:6',
            'newpass' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            // echo('no passo el validar');
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput()
                ->with('passFail', 'No se pudo actualizar la contraseña');
        } else {
            if (Hash::check($request->input('oldpass'), $currentuser->password)) {
                // echo("password sin hasehar:".$currentuser->password."<br>");
                $currentuser->password = Hash::make($request->input('newpass'));
                $currentuser->save();
                // echo("Pass antiguo:".$request->input('oldpass')."<br>");
                // echo("Pass nuevo desde formulario:".$request->input('newpass')."<br>");
                // echo("Pass nuevo hasheado :".$currentuser->password)."<br>";
                return redirect()

                    ->back()
                    ->with('statusPasswordOk', 'Contraseña modificada!');
            } else {
                echo ('mal');
                return redirect()
                    ->back()
                    ->with('statusPasswordFail', 'La contraseña actual no coincide.');
            }
        }
    }

    public function update_avatar(Request $request)
    {

        // Handle the user upload of avatar
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/avatars/' . $filename));

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }

        return redirect('/user/home');
    }

    public function cambiarOpinion(Request $data)
    {

        //Obtenemos la id de usuario
        $id = Auth::user()->id;

        //Guardamos la id que viene del modal
        $id_valoracion = $data->except('_token', 'opinion_new');

        //Guardamos el nuevo comentario
        $comentario = $data->except(['_token', 'id-valoracion']);
        // echo ($id_valoracion['id-valoracion']);

        DB::table('valoracion')
            ->join('cliente', 'valoracion.cliente_id', '=', 'cliente.cliente_id')
            ->join('users', 'cliente.usuario_id', '=', 'users.id')
            ->where('users.id', '=', $id)
            ->where('valoracion.valoracion_id', '=', $id_valoracion['id-valoracion'])
            ->update(
                ['opinion' => $comentario['opinion_new']]
            );
        return redirect()
            ->back()
            ->with('statusPasswordOk', 'Contraseña modificada!');
    }

    function eliminarOpinion($id)
    {
        //En proceso
        DB::table('valoracion')
            ->where('valoracion_id', '=', $$id)
            ->delete();

            return redirect()
            ->back()
            ->with('statusOk', 'Valoracion eliminada');
    }
}
