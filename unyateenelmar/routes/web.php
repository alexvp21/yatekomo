<?php
use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'mainController@getIndexView');

Route::get('/barcos', 'mainController@getBarcosView');

Route::get('/quienesomos', 'mainController@getQuienView');

Route::get('/contacto', 'mainController@getContactView');

Route::get('/productos', 'queryController@getProductos');

//Rutas para la zona de usuario

Route::get('/user/home','userController@getIndexView')->middleware('auth');

Route::post('profile', 'userController@update_avatar')->middleware('auth');

Route::get('/user/favoritos','userController@getFavView')->middleware('auth');

Route::get('/user/mispagos','userController@getPagosView')->middleware('auth');

Route::get('/user/misreservas','userController@getReservasView')->middleware('auth');

Route::get('/user/misvaloraciones','userController@getValoracionView')->middleware('auth');

Route::post('userUpdate','userController@actualizarDatos')->middleware('auth');

Route::post('userUpdatePassword','userController@resetPassword')->middleware('auth');

Route::post('/actualizarOpinion','userController@cambiarOpinion');

//Rutas para la zona de admin

Route::get('/admin/barcos','adminController@getIndexView')->middleware('auth', 'checkAdmin');

Route::get('/admin/usuarios','adminController@getUsuariosView')->middleware('auth', 'checkAdmin');

Route::get('/admin/valoraciones','adminController@getValoracionView')->middleware('auth', 'checkAdmin');

Route::get('/admin/incidencias','adminController@getIncidenciasView')->middleware('auth', 'checkAdmin');

Route::get('/admin/reservas','adminController@getReservasView')->middleware('auth', 'checkAdmin');

//Rutas para la zona de login

// Route::post('/register','auth\RegisterController@getRegisterView');

// Route::post('/login','auth\LoginController@getLoginView');


// --------  Peticiones BBDD  --------- //

// -------- Peticiones WEB ------------ //

Route::post('/search', 'mainController@inicioSearch');

Route::post('/advancedSearch','mainController@barcoSearch');

Route::post('/buscar','mainController@soloTipo');

// --------- Peticiones Zona Usuarios --- //
// Nos mostrará el formulario de login.
// Route::get('/login', 'AuthController@showLogin');

// // --------- Peticiones Zona Usuarios --- //

Auth::routes();

Route::post('/register', 'transactionController@registroCliente')->middleware('guest');

Route::get('/barcoSpecs/{id}', 'adminController@barcoSpec')->middleware('auth', 'checkAdmin');

Route::get('/reservaSpec/{id}', 'adminController@reservaSpec')->middleware('auth', 'checkAdmin');

Route::post('/modBarco', 'adminController@modifyBarco')->middleware('auth', 'checkAdmin');

Route::get('/clienteSpecs/{id}', 'adminController@clienteSpec')->middleware('auth', 'checkAdmin');

Route::get('/clienteSpecs/{id}', 'adminController@clienteSpec')->middleware('auth', 'checkAdmin');

Route::post('/modCliente', 'adminController@modifyCliente')->middleware('auth', 'checkAdmin');

Route::get('/delbarco/{id}', 'adminController@borrarBarco')->middleware('auth', 'checkAdmin');

Route::post('/reservar', 'mainController@reservarBarco')->middleware('auth');

Route::get('/delusuario/{id}', 'adminController@borrarUsuario')->middleware('auth', 'checkAdmin');

Route::get('/delvaloracion/{id}', 'adminController@borrarValoracion')->middleware('auth', 'checkAdmin');

Route::get('/delincidencia/{id}', 'adminController@borrarIncidencia')->middleware('auth', 'checkAdmin');

Route::get('/delreserva/{id}', 'adminController@borrarReserva')->middleware('auth', 'checkAdmin');

Route::post('/insBarco', 'adminController@insertBarco')->middleware('auth', 'checkAdmin');

Route::post('/insIncidencia', 'adminController@insertIncidencia')->middleware('auth', 'checkAdmin');

Route::post('/modReserva', 'adminController@modifyReserva')->middleware('auth', 'checkAdmin');

Route::post('/reservarBarco', 'mainController@reservar')->middleware('auth');