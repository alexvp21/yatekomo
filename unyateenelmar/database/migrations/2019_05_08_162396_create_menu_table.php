<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla menu */
        Schema::create('menu', function (Blueprint $table) {
            $table->bigIncrements('menu_id');

            $table->text('nombre_menu');
            $table->float('precioPorPersona');
            $table->text('alergenos');
            $table->text('entrantesMenu');
            $table->text('primerosMenu');
            $table->text('segundosMenu');
            $table->text('postresMenu');
            $table->text('bebidasMenu');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
    }
}
