<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarcoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla barcos */
        Schema::create('barco', function (Blueprint $table) {
            $table->bigIncrements('barco_id');
            $table->text('nombre_barco');
            $table->float('eslora');
            $table->integer('camarote');
            $table->text('tipo');
            $table->integer('pasajero_max');
            $table->integer('anyo');
            $table->float('precio_dia');
            $table->integer('plazas_cama');
            $table->float('fianza');
            $table->integer('voto_positivo');
            $table->integer('voto_negativo');
            $table->text('imagen_portada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barco');
    }
}
