<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla reserva */
        Schema::create('reserva', function (Blueprint $table) {

            $table->bigIncrements('reserva_id');

            $table->date('fecha_inicio');
            $table->float('precioTotal');
            $table->text('estado');
            $table->integer('numPasajeros');

            //Agrega relacion 1 a N
            $table->bigInteger('trayecto_id')->unsigned();  
            $table->foreign('trayecto_id')
            ->references('trayecto_id')->on('trayecto')
            ->onDelete('cascade');

            //Agrega relacion 1 a N
            $table->bigInteger('barco_id')->unsigned();  
            $table->foreign('barco_id')
            ->references('barco_id')->on('barco')
            ->onDelete('cascade');

            //Agrega relacion 1 a N
            $table->bigInteger('chef_id')->unsigned();  
            $table->foreign('chef_id')
            ->references('chef_id')->on('chef')
            ->onDelete('cascade');

            //Agrega relacion 1 a N
            $table->bigInteger('cliente_id')->unsigned();  
            $table->foreign('cliente_id')
            ->references('cliente_id')->on('cliente')
            ->onDelete('cascade');

            //Agrega relacion 1 a 1
            $table->bigInteger('factura_id')->unsigned();  
            $table->foreign('factura_id')
            ->references('factura_id')->on('factura')
            ->onDelete('cascade');

            //Agrega relacion 1 a 1
            $table->bigInteger('menu_id')->unsigned();  
            $table->foreign('menu_id')
            ->references('menu_id')->on('menu')
            ->onDelete('cascade');

            //Agrega relacion 1 a N
            $table->bigInteger('capitan_id')->unsigned();  
            $table->foreign('capitan_id')
            ->references('capitan_id')->on('capitan')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserva');
    }
}
