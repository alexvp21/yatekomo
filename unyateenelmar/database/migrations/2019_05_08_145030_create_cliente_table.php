<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla clientes */
        Schema::create('cliente', function (Blueprint $table) {

            //Definimos la clave primaria
            $table->bigIncrements('cliente_id');
            $table->date('fechaNacimiento');
            $table->string('sexo');
            $table->string('dni');
            $table->integer('telefono');
            $table->integer('puntos');
            $table->text('rol');

            $table->bigInteger('usuario_id')->unsigned();
            //Agrega relacion
            $table->foreign('usuario_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
