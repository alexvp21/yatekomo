<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoritoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla favorito */
        Schema::create('favorito', function (Blueprint $table) {
            $table->bigIncrements('favorito_id');

            //Agrega relacion 1 a N
            $table->bigInteger('cliente_id')->unsigned();  
            $table->foreign('cliente_id')->references('cliente_id')->on('cliente');
       
            //Agrega relacion 1 a N
            $table->bigInteger('barco_id')->unsigned();  
            $table->foreign('barco_id')
            ->references('barco_id')->on('barco')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorito');
    }
}
