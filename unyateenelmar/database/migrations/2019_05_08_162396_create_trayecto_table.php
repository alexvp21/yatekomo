<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrayectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla trayecto */
        Schema::create('trayecto', function (Blueprint $table) {
            $table->bigIncrements('trayecto_id');

            $table->text('nombre_trayecto');
            $table->float('precioTrayecto');
            $table->integer('horas');
            $table->text('url_Imagen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trayecto');
    }
}
