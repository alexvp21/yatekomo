<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagenBarcoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla imagenes barco */
        Schema::create('imagen_barco', function (Blueprint $table) {
            $table->bigIncrements('imagen_barco_id');
            $table->text('url');

            //Agrega relacion 1 a N
            $table->bigInteger('barco_id')->unsigned();  
            $table->foreign('barco_id')
            ->references('barco_id')->on('barco')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagen_barco');
    }
}
