<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla incidencia */
        Schema::create('incidencia', function (Blueprint $table) {
            $table->bigIncrements('incidencias_id');
            $table->dateTime('hora_inicidencia');
            $table->text('info',1000);

            //Agrega relacion 1 a N
            $table->bigInteger('barco_id')->unsigned();  
            $table->foreign('barco_id')->references('barco_id')->on('barco');

            //Agrega relacion 1 a N
            $table->bigInteger('capitan_id')->unsigned();  
            $table->foreign('capitan_id')
            ->references('capitan_id')->on('capitan')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidencia');
    }
}
