<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapitanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla capitan */
        Schema::create('capitan', function (Blueprint $table) {
            $table->bigIncrements('capitan_id');

            $table->string('nombre_capitan');
            $table->string('apellidos');
            $table->text('dni');
            $table->integer('telefono');
            $table->text('email');
            $table->text('licencia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capitan');
    }
}
