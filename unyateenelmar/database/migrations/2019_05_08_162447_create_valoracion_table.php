<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValoracionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla valoracion */
        Schema::create('valoracion', function (Blueprint $table) {
            $table->bigIncrements('valoracion_id');
            $table->text('opinion',1000);

            //Agrega relacion 1 a N
            $table->bigInteger('cliente_id')->unsigned();  
            $table->foreign('cliente_id')
            ->references('cliente_id')->on('cliente')
            ->onDelete('cascade');

            //Agrega relacion 1 a N
            $table->bigInteger('barco_id')->unsigned();  
            $table->foreign('barco_id')
            ->references('barco_id')->on('barco')
            ->onDelete('cascade');

            //Agrega relacion 1 a 1
            $table->bigInteger('reserva_id')->unsigned();
            $table->foreign('reserva_id')
            ->references('reserva_id')->on('reserva')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valoracion');
    }
}
