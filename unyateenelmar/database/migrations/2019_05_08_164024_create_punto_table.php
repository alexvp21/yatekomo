<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuntoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla punto */
        Schema::create('punto', function (Blueprint $table) {
            $table->bigIncrements('punto_id');

            $table->text('latitud', 20);
            $table->text('longitud', 20);
            $table->text('nombre_punto');

            //Agrega relacion 1 a N
            $table->bigInteger('trayecto_id')->unsigned();  
            $table->foreign('trayecto_id')
            ->references('trayecto_id')->on('trayecto')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('punto');
    }
}
