<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasajerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Tabla pasajeros */
        Schema::create('pasajeros', function (Blueprint $table) {
            $table->bigIncrements('pasajero_id');

            $table->string('nombre');
            $table->string('apellidos');
            $table->date('fechaNacimiento');
            $table->string('dni');

            //Agrega relacion 1 a N
            $table->bigInteger('reserva_id')->unsigned();  
            $table->foreign('reserva_id')
            ->references('reserva_id')->on('reserva')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasajeros');
    }
}
