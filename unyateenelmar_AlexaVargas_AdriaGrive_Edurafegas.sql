-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 30-05-2019 a las 15:50:32
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `yatekomo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anyos`
--

CREATE TABLE `anyos` (
  `id` int(11) NOT NULL,
  `anyo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `anyos`
--

INSERT INTO `anyos` (`id`, `anyo`) VALUES
(1, 1990),
(2, 1991),
(3, 1992),
(4, 1993),
(5, 1994),
(6, 1995),
(7, 1996),
(8, 1997),
(9, 1998),
(10, 1999),
(11, 2000),
(12, 2001),
(13, 2002),
(14, 2003),
(15, 2004),
(16, 2005),
(17, 2006),
(18, 2007),
(19, 2008),
(20, 2009),
(21, 2010),
(22, 2011),
(23, 2012),
(24, 2013),
(25, 2014),
(26, 2015),
(27, 2016),
(28, 2017),
(29, 2018),
(30, 2019),
(31, 2020);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barco`
--

CREATE TABLE `barco` (
  `barco_id` bigint(20) UNSIGNED NOT NULL,
  `nombre_barco` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `eslora` double(8,2) NOT NULL,
  `camarote` int(11) NOT NULL,
  `tipo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pasajero_max` int(11) NOT NULL,
  `anyo` int(11) NOT NULL,
  `precio_dia` double(8,2) NOT NULL,
  `plazas_cama` int(11) NOT NULL,
  `fianza` double(8,2) NOT NULL,
  `voto_positivo` int(11) NOT NULL,
  `voto_negativo` int(11) NOT NULL,
  `imagen_portada` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `barco`
--

INSERT INTO `barco` (`barco_id`, `nombre_barco`, `eslora`, `camarote`, `tipo`, `pasajero_max`, `anyo`, `precio_dia`, `plazas_cama`, `fianza`, `voto_positivo`, `voto_negativo`, `imagen_portada`) VALUES
(1, 'Yate1', 10.25, 4, 'yate', 8, 2002, 250.50, 5, 50.50, 50, 30, '/img/img-inicio-2.jpg'),
(2, 'Velero1', 10.25, 4, 'velero', 7, 2005, 250.50, 5, 50.50, 50, 30, '/img/img-inicio-3.jpg'),
(3, 'Goleta1', 10.25, 4, 'goletas', 6, 1980, 250.50, 5, 50.50, 50, 30, '/img/img-inicio-5.jpg'),
(4, 'Catamaranes', 10.25, 4, 'catamaranes', 5, 1989, 250.50, 5, 50.50, 50, 30, '/img/img-inicio-4.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `capitan`
--

CREATE TABLE `capitan` (
  `capitan_id` bigint(20) UNSIGNED NOT NULL,
  `nombre_capitan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `licencia` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `capitan`
--

INSERT INTO `capitan` (`capitan_id`, `nombre_capitan`, `apellidos`, `dni`, `telefono`, `email`, `licencia`, `created_at`, `updated_at`) VALUES
(1, 'Capitan1', 'Apellido1', '20000000-A', 666111444, 'capitan1@yatekomo.com', 'Licencia1', NULL, NULL),
(2, 'Capitan2', 'Apellido2', '20000000-B', 655111334, 'capitan2@yatekomo.com', 'Licencia2', NULL, NULL),
(3, 'Capitan3', 'Apellido3', '30000000-B', 655111334, 'capitan3@yatekomo.com', 'Licencia3', NULL, NULL),
(4, 'Capitan4', 'Apellido4', '40000000-B', 655111334, 'capitan4@yatekomo.com', 'Licencia4', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chef`
--

CREATE TABLE `chef` (
  `chef_id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `chef`
--

INSERT INTO `chef` (`chef_id`, `nombre`, `apellidos`, `dni`, `telefono`) VALUES
(1, 'Cocinero1', 'Apellido1', '10000000-A', 686121212),
(2, 'Cocinero1', 'Apellido2', '10000000-B', 626335544),
(3, 'Cocinero1', 'Apellido3', '10000000-C', 616232342),
(4, 'Cocinero1', 'Apellido4', '10000000-D', 627343452),
(5, 'Cocinero1', 'Apellido5', '10000000-F', 623452343);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `puntos` int(11) NOT NULL,
  `rol` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`cliente_id`, `fechaNacimiento`, `sexo`, `dni`, `telefono`, `puntos`, `rol`, `usuario_id`) VALUES
(1, '2000-05-31', 'hombre', 'y545345t', 672716202, 0, 'administrador', 1),
(2, '2000-05-31', 'hombre', 'y545345t', 672716202, 0, 'usuario', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `factura_id` bigint(20) UNSIGNED NOT NULL,
  `forma_de_pago` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` double(8,2) NOT NULL,
  `detalles` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_de_compra` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`factura_id`, `forma_de_pago`, `precio`, `detalles`, `fecha_de_compra`) VALUES
(1, 'tarjeta', 200.00, 'Nombre del barco,numero pasajeros etc etc etc', '2019-05-30'),
(2, 'paypal', 500.00, 'Nombre del barco,numero pasajeros etc etc etc', '2019-05-30'),
(3, 'tarjeta', 100.00, 'Nombre del barcqweqwo,numero pasajeros etc etc etc', '2019-05-30'),
(4, 'tarjeta', 50.00, 'Nombre del barco2,numero pasajeros etc etc etc', '2019-05-30'),
(5, 'paypal', 11121.00, 'Nombre del barco3,numero pasajeros etc etc etc', '2019-05-30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favorito`
--

CREATE TABLE `favorito` (
  `favorito_id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `barco_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `favorito`
--

INSERT INTO `favorito` (`favorito_id`, `cliente_id`, `barco_id`) VALUES
(1, 1, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_barco`
--

CREATE TABLE `imagen_barco` (
  `imagen_barco_id` bigint(20) UNSIGNED NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `barco_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencia`
--

CREATE TABLE `incidencia` (
  `incidencias_id` bigint(20) UNSIGNED NOT NULL,
  `hora_inicidencia` datetime NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `barco_id` bigint(20) UNSIGNED NOT NULL,
  `capitan_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `incidencia`
--

INSERT INTO `incidencia` (`incidencias_id`, `hora_inicidencia`, `info`, `barco_id`, `capitan_id`) VALUES
(1, '2019-06-24 22:40:09', 'No quedan cervezas', 1, 1),
(2, '2019-06-25 22:40:09', 'Se me ha roto el motor', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `nombre_menu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `precioPorPersona` double(8,2) NOT NULL,
  `alergenos` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `entrantesMenu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `primerosMenu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `segundosMenu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `postresMenu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bebidasMenu` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`menu_id`, `nombre_menu`, `precioPorPersona`, `alergenos`, `entrantesMenu`, `primerosMenu`, `segundosMenu`, `postresMenu`, `bebidasMenu`) VALUES
(1, 'Menu Crucero Los 4 vientos', 20.00, 'Contiene: Marisco, gluten,trazas cacahuete.', ' Chips de plátano con dip de frijoles negros', 'esferas crujientes de yuca, pollo y mojo verde | cóctel de langostinos, kétchup de tamarillo, tabasco, mango verde y casabe de yuca', 'magret de pato en hoja de plátano', 'panna cotta de lichis y coulis de flor de Jamaica', 'Marques de alella (Gran reserva), agua, refrescos'),
(2, 'Menu El Veggie', 35.00, 'Contiene: Marisco, gluten,trazas cacahuete, trazas de limón', 'Crema de habitas y menta', 'trinxat de patata, espigalls y tempeh | alcachofas maceradas en naranja', 'seta maitake a la plancha | ravioli relleno de huevos eco y alcachofa', 'carquinyoli al sorbete de moscatel y trufa de chocolate', 'Vino de la casa \"SOMOS\", agua, refrescos'),
(3, 'Menu El ultimo Mono 3', 50.00, 'Contiene: Carne, gluten,trazas mora roja, queso variedad Parmesano', 'Croquetas de Leopoldo | Tartar vegetal con mozzarella tebia | Micuit del narcís con frutos secos y confitura de higo', 'Salmón marinado con cremoso de confitados | Brandada de bacalao, kalamata y xips de plátano | Canelón de jabalo de caza con bechamel trufada, foie y setas', 'Costillar duroc lacado con patata al horno | Mar y montaña de pulpo teriyaki', 'Mar rojo con queso y arandanos al polvo de trufa', 'Gran periñón, Vega sicilia unico (1720), agua y refrescos'),
(4, 'Sin menu', 0.00, 'Sin menu', 'Sin menu', 'Sin menu', 'Sin menu', 'Sin menu', 'Sin menu');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_08_145030_create_cliente_table', 1),
(4, '2019_05_08_162200_create_barco_table', 1),
(5, '2019_05_08_162201_create_favorito_table', 1),
(6, '2019_05_08_162395_create_chef_table', 1),
(7, '2019_05_08_162396_create_factura_table', 1),
(8, '2019_05_08_162396_create_menu_table', 1),
(9, '2019_05_08_162396_create_trayecto_table', 1),
(10, '2019_05_08_162397_create_capitan_table', 1),
(11, '2019_05_08_162398_create_reserva_table', 1),
(12, '2019_05_08_162399_create_incidencia_table', 1),
(13, '2019_05_08_162447_create_valoracion_table', 1),
(14, '2019_05_08_163340_create_imagen_barco_table', 1),
(15, '2019_05_08_163726_create_pasajeros_table', 1),
(16, '2019_05_08_164024_create_punto_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasajeros`
--

CREATE TABLE `pasajeros` (
  `pasajero_id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `dni` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reserva_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pasajeros`
--

INSERT INTO `pasajeros` (`pasajero_id`, `nombre`, `apellidos`, `fechaNacimiento`, `dni`, `reserva_id`) VALUES
(1, 'Pasajero1', 'Apellido1', '0000-00-00', '30000000-A', 1),
(2, 'Pasajero2', 'Apellido2', '0000-00-00', '30000000-B', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `punto`
--

CREATE TABLE `punto` (
  `punto_id` bigint(20) UNSIGNED NOT NULL,
  `latitud` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitud` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_punto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `trayecto_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `punto`
--

INSERT INTO `punto` (`punto_id`, `latitud`, `longitud`, `nombre_punto`, `trayecto_id`) VALUES
(1, '2.180804', '41.362882', 'Barcelona', 1),
(2, '2.843142', '41.686551', 'Lloret de mar', 1),
(3, '3.235591', '41.893342', 'Llafranch', 1),
(4, '3.312806', '42.315259', 'Port Lligat', 1),
(5, '3.030149', '42.696681', 'Argelès Plage', 1),
(6, '3.038574', '42.787809', 'Le barcarès', 1),
(7, '2.180804', '41.362882', 'Barcelona', 2),
(8, '1.226625', '41.097266', 'Tarragona', 2),
(9, '2.515958', '39.690904', 'Bañalbufar', 2),
(10, '2.180804', '41.362882', 'Barcelona', 3),
(11, '-0.316544', '39.455035', 'Valencia', 3),
(12, '0', '0', 'Libre', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `reserva_id` bigint(20) UNSIGNED NOT NULL,
  `fecha_inicio` date NOT NULL,
  `precioTotal` double(8,2) NOT NULL,
  `estado` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `numPasajeros` int(11) NOT NULL,
  `trayecto_id` bigint(20) UNSIGNED NOT NULL,
  `barco_id` bigint(20) UNSIGNED NOT NULL,
  `chef_id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `factura_id` bigint(20) UNSIGNED NOT NULL,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `capitan_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`reserva_id`, `fecha_inicio`, `precioTotal`, `estado`, `numPasajeros`, `trayecto_id`, `barco_id`, `chef_id`, `cliente_id`, `factura_id`, `menu_id`, `capitan_id`) VALUES
(1, '2019-06-22', 1.00, 'pendiente', 2, 1, 1, 1, 1, 1, 1, 1),
(2, '2019-06-23', 2.00, 'pendiente', 4, 2, 2, 2, 2, 2, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trayecto`
--

CREATE TABLE `trayecto` (
  `trayecto_id` bigint(20) UNSIGNED NOT NULL,
  `nombre_trayecto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `precioTrayecto` double(8,2) NOT NULL,
  `horas` int(11) NOT NULL,
  `url_Imagen` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `trayecto`
--

INSERT INTO `trayecto` (`trayecto_id`, `nombre_trayecto`, `precioTrayecto`, `horas`, `url_Imagen`) VALUES
(1, 'Costa Brava', 1050.00, 8, '/uploads/trayectos/trayecto1.png'),
(2, 'Navega Ibiza', 820.00, 6, '/uploads/trayectos/trayecto2.png'),
(3, 'Cartagena', 600.00, 4, '/uploads/trayectos/trayecto3.png'),
(4, 'A tu gusto', 1700.00, 8, '/uploads/trayectos/trayecto4.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', '$2y$10$.r5jPGBp68ZXH4hJ0jY7YurJufPJv.z7dcWf6vvllI0LseXjeU8z6', 'administrador@yatekomo.com', 'default.jpg', 'FnOVkZrJS1kZJdayRl4HmV85qaEnn9uRAyxVzf12raQ178Dr6JpA14ToSlTp', NULL, NULL),
(2, 'Usuario', '$2y$10$BMH4rXkk/k6AFWGt5mCHveu7naWMy6xFQV0un.B8UhnAb6ibBZEy.', 'usuario@yatekomo.com', 'default.jpg', 'tC0j9G6QB6c1HGwlqxe7D8W2dZ8zcIYjbC7acbvXS8GV5HKcY5L8BcqirqaL', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valoracion`
--

CREATE TABLE `valoracion` (
  `valoracion_id` bigint(20) UNSIGNED NOT NULL,
  `opinion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `barco_id` bigint(20) UNSIGNED NOT NULL,
  `reserva_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `valoracion`
--

INSERT INTO `valoracion` (`valoracion_id`, `opinion`, `cliente_id`, `barco_id`, `reserva_id`) VALUES
(1, 'Opinion numero 1: Se come muy bien', 1, 1, 1),
(2, 'Opinion numero 2: Todo estupendo!', 2, 2, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anyos`
--
ALTER TABLE `anyos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `barco`
--
ALTER TABLE `barco`
  ADD PRIMARY KEY (`barco_id`);

--
-- Indices de la tabla `capitan`
--
ALTER TABLE `capitan`
  ADD PRIMARY KEY (`capitan_id`);

--
-- Indices de la tabla `chef`
--
ALTER TABLE `chef`
  ADD PRIMARY KEY (`chef_id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cliente_id`),
  ADD KEY `cliente_usuario_id_foreign` (`usuario_id`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`factura_id`);

--
-- Indices de la tabla `favorito`
--
ALTER TABLE `favorito`
  ADD PRIMARY KEY (`favorito_id`),
  ADD KEY `favorito_cliente_id_foreign` (`cliente_id`),
  ADD KEY `favorito_barco_id_foreign` (`barco_id`);

--
-- Indices de la tabla `imagen_barco`
--
ALTER TABLE `imagen_barco`
  ADD PRIMARY KEY (`imagen_barco_id`),
  ADD KEY `imagen_barco_barco_id_foreign` (`barco_id`);

--
-- Indices de la tabla `incidencia`
--
ALTER TABLE `incidencia`
  ADD PRIMARY KEY (`incidencias_id`),
  ADD KEY `incidencia_barco_id_foreign` (`barco_id`),
  ADD KEY `incidencia_capitan_id_foreign` (`capitan_id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pasajeros`
--
ALTER TABLE `pasajeros`
  ADD PRIMARY KEY (`pasajero_id`),
  ADD KEY `pasajeros_reserva_id_foreign` (`reserva_id`);

--
-- Indices de la tabla `punto`
--
ALTER TABLE `punto`
  ADD PRIMARY KEY (`punto_id`),
  ADD KEY `punto_trayecto_id_foreign` (`trayecto_id`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`reserva_id`),
  ADD KEY `reserva_trayecto_id_foreign` (`trayecto_id`),
  ADD KEY `reserva_barco_id_foreign` (`barco_id`),
  ADD KEY `reserva_chef_id_foreign` (`chef_id`),
  ADD KEY `reserva_cliente_id_foreign` (`cliente_id`),
  ADD KEY `reserva_factura_id_foreign` (`factura_id`),
  ADD KEY `reserva_menu_id_foreign` (`menu_id`),
  ADD KEY `reserva_capitan_id_foreign` (`capitan_id`);

--
-- Indices de la tabla `trayecto`
--
ALTER TABLE `trayecto`
  ADD PRIMARY KEY (`trayecto_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `valoracion`
--
ALTER TABLE `valoracion`
  ADD PRIMARY KEY (`valoracion_id`),
  ADD KEY `valoracion_cliente_id_foreign` (`cliente_id`),
  ADD KEY `valoracion_barco_id_foreign` (`barco_id`),
  ADD KEY `valoracion_reserva_id_foreign` (`reserva_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anyos`
--
ALTER TABLE `anyos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `barco`
--
ALTER TABLE `barco`
  MODIFY `barco_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `capitan`
--
ALTER TABLE `capitan`
  MODIFY `capitan_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `chef`
--
ALTER TABLE `chef`
  MODIFY `chef_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `cliente_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `factura_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `favorito`
--
ALTER TABLE `favorito`
  MODIFY `favorito_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `imagen_barco`
--
ALTER TABLE `imagen_barco`
  MODIFY `imagen_barco_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `incidencia`
--
ALTER TABLE `incidencia`
  MODIFY `incidencias_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `pasajeros`
--
ALTER TABLE `pasajeros`
  MODIFY `pasajero_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `punto`
--
ALTER TABLE `punto`
  MODIFY `punto_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
  MODIFY `reserva_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `trayecto`
--
ALTER TABLE `trayecto`
  MODIFY `trayecto_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `valoracion`
--
ALTER TABLE `valoracion`
  MODIFY `valoracion_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_usuario_id_foreign` FOREIGN KEY (`usuario_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `favorito`
--
ALTER TABLE `favorito`
  ADD CONSTRAINT `favorito_barco_id_foreign` FOREIGN KEY (`barco_id`) REFERENCES `barco` (`barco_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favorito_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`cliente_id`);

--
-- Filtros para la tabla `imagen_barco`
--
ALTER TABLE `imagen_barco`
  ADD CONSTRAINT `imagen_barco_barco_id_foreign` FOREIGN KEY (`barco_id`) REFERENCES `barco` (`barco_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `incidencia`
--
ALTER TABLE `incidencia`
  ADD CONSTRAINT `incidencia_barco_id_foreign` FOREIGN KEY (`barco_id`) REFERENCES `barco` (`barco_id`),
  ADD CONSTRAINT `incidencia_capitan_id_foreign` FOREIGN KEY (`capitan_id`) REFERENCES `capitan` (`capitan_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `pasajeros`
--
ALTER TABLE `pasajeros`
  ADD CONSTRAINT `pasajeros_reserva_id_foreign` FOREIGN KEY (`reserva_id`) REFERENCES `reserva` (`reserva_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `punto`
--
ALTER TABLE `punto`
  ADD CONSTRAINT `punto_trayecto_id_foreign` FOREIGN KEY (`trayecto_id`) REFERENCES `trayecto` (`trayecto_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `reserva_barco_id_foreign` FOREIGN KEY (`barco_id`) REFERENCES `barco` (`barco_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reserva_capitan_id_foreign` FOREIGN KEY (`capitan_id`) REFERENCES `capitan` (`capitan_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reserva_chef_id_foreign` FOREIGN KEY (`chef_id`) REFERENCES `chef` (`chef_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reserva_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`cliente_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reserva_factura_id_foreign` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`factura_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reserva_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reserva_trayecto_id_foreign` FOREIGN KEY (`trayecto_id`) REFERENCES `trayecto` (`trayecto_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `valoracion`
--
ALTER TABLE `valoracion`
  ADD CONSTRAINT `valoracion_barco_id_foreign` FOREIGN KEY (`barco_id`) REFERENCES `barco` (`barco_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `valoracion_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`cliente_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `valoracion_reserva_id_foreign` FOREIGN KEY (`reserva_id`) REFERENCES `reserva` (`reserva_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
